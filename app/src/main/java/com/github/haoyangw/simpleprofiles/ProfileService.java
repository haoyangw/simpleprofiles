package com.github.haoyangw.simpleprofiles;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class ProfileService extends Service {
    private Context mContext = null;
    private boolean bg = false;
    final int notId = 91;
    ArrayList<Integer> times = new ArrayList<>();
    boolean rep;
    int id;

    public ProfileService() {

    }
    /**public ProfileService(Context context) {
        mContext = context;
    }**/

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mContext = this;
        final String prof = (String)intent.getExtras().get("name");
        bg = intent.getExtras().getBoolean("bg");
        if (prof != null) {
            times = intent.getIntegerArrayListExtra("time");
            if(times != null) {
                rep = intent.getBooleanExtra("rep", false);
                id = intent.getIntExtra("id", -1);
                if(!rep) {
                    Log.i("ProfileService", "'" + prof + "' alarm is one-time at " + times.get(0) + ":" + times.get(1));
                }
            } else {
                Log.i("ProfileService", "Could not get trigger time :(");
            }
            final File configs = new File(getFilesDir().getParent() + "/shared_prefs/" + prof.toLowerCase() + ".xml");
            if(configs.exists()) {
                Log.i("ProfileService", "Loading profile '" + prof + "'");
                Toast.makeText(this, "Loading profile '" + prof + "'", Toast.LENGTH_SHORT).show();
                boolean notify = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_notify", false);
                if(notify) {
                    final NotificationManager notif = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                    final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                    final Intent act = new Intent(this, ProfilesActivity.class);
                    PendingIntent resAct = PendingIntent.getActivity(this, 0, act, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentTitle("SimpleProfiles")
                            .setContentText("Switched to profile '" + prof + "'")
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(resAct);
                    notif.notify(notId, builder.build());
                }
                Thread exec = new Thread(new LoadProf(prof));
                try {
                    exec.start();
                    //exec.join();
                }catch(Throwable e) {
                    Log.e("ProfileService", Log.getStackTraceString(e));
                }
            } else
                Log.e("ProfileService", "ERROR: No such profile: '" + prof + "'");
        }
        stopSelf(startId);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    public final class LoadProf implements Runnable {
        private String loadable = "";
        LoadProf(String profile) {
            loadable = profile;
        }
        @SuppressLint({"CommitPrefEdits", "NewApi"})
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            try {
                final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(mContext);
                final String profs = shared.getString("pref_profiles", "");
                if(!profs.equals("")) {
                    String[] profList = profs.split(",");
                    final int pos = Arrays.asList(profList).indexOf(loadable);
                    boolean[] selected = new boolean[profList.length];
                    Arrays.fill(selected, false);
                    selected[pos] = true;
                    final StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < selected.length; i++) {
                        sb.append(String.valueOf(selected[i]));
                        if (i != selected.length - 1) {
                            sb.append(",");
                        }
                    }
                    final SharedPreferences.Editor update = shared.edit();
                    update.putString("selected", sb.toString());
                    update.commit();
                    if(bg) {
                        Intent refresh = new Intent();
                        refresh.setAction("haoyangw.simpleprofiles.refresh");
                        sendBroadcast(refresh);
                    }
                }
                final SharedPreferences pref = getSharedPreferences(loadable.toLowerCase(), MODE_PRIVATE);
                final boolean wifi = pref.getBoolean("pref_wifi", false);
                final boolean md = pref.getBoolean("pref_md", false);
                final boolean bt = pref.getBoolean("pref_bt", false);
                int loc = 1;
                boolean gps = false;
                if(Build.VERSION.SDK_INT >= 19)
                    loc = Integer.parseInt(pref.getString("pref_gps", "1"));
                else
                    gps = pref.getBoolean("pref_gps_prekk", false);
                final boolean as = pref.getBoolean("pref_as", false);
                final boolean apm = pref.getBoolean("pref_apm", false);
                final int sound = Integer.parseInt(pref.getString("pref_sound", "1"));
                final boolean hs = pref.getBoolean("pref_hs", false);
                Log.i("ProfileService", "Wifi: " + String.valueOf(wifi) + " Mobile Data: " + String.valueOf(md) +
                        " Bluetooth: " + String.valueOf(bt) + " Location: " + String.valueOf(loc) +
                        " Auto-sync: " + String.valueOf(as) + " Airplane Mode: " + String.valueOf(apm) +
                        " Mobile Hotspot: " + String.valueOf(hs) + " Sound: " + String.valueOf(sound));
                //final Functions func = new Functions();
                //Airplane Mode
                setAPM(apm);
                //Wifi
                final WifiManager wifiman = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
                if(wifiman.isWifiEnabled() != wifi)
                    wifiman.setWifiEnabled(wifi);
                //Mobile Data
                setMobileData(mContext, md);
                //func.setMobileData(mContext, md);
                //Bluetooth
                setBT(bt);
                //func.setBT(bt);
                //Mobile Hotspot
                try {
                    final WifiConfiguration wifi_config = null;
                    final Method toggle_hs = wifiman.getClass()
                            .getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
                    toggle_hs.invoke(wifiman, wifi_config, hs);
                }catch(Throwable e) {
                    Log.e("ProfileService", Log.getStackTraceString(e));
                }
                //GPS
                //TODO: Possibly detect GPS enabled/mode so that no need to do redundant work?
                if(Build.VERSION.SDK_INT >= 19)
                    setGPS(loc);
                    //func.setGPS(mContext, loc);
                else
                    setGPS(gps);
                    //func.setGPS(mContext, gps);
                //Auto Sync
                if(ContentResolver.getMasterSyncAutomatically() != as)
                    ContentResolver.setMasterSyncAutomatically(as);
                //Sound
                final AudioManager audio = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
                if(audio.getRingerMode() != sound)
                    audio.setRingerMode(sound);
                if(times != null && rep) {
                    Log.i("ProfileService", "Setting repeating alarm at " +
                            times.get(0) + ":" + times.get(1) + " for '" + loadable + "'");
                    final Intent serv = new Intent(mContext, ProfileService.class);
                    serv.putExtra("name", loadable);
                    serv.putExtra("bg", false);
                    serv.putExtra("time", times);
                    serv.putExtra("rep", true);
                    serv.putExtra("id", id);
                    serv.setData(Uri.parse("time:" + String.valueOf(id)));
                    final PendingIntent load = PendingIntent.getService(mContext, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                    final Calendar c = Calendar.getInstance();
                    c.setTimeInMillis(System.currentTimeMillis());
                    c.set(Calendar.HOUR_OF_DAY, times.get(0));
                    c.set(Calendar.MINUTE, times.get(1));
                    c.set(Calendar.SECOND, 0);
                    c.set(Calendar.MILLISECOND, 0);
                    c.add(Calendar.WEEK_OF_YEAR, 1);
                    final AlarmManager alarms = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                    final int sdk = Build.VERSION.SDK_INT;
                    if (sdk >= 19 && sdk < 23) // TODO: Settings pref to not wake during Doze
                        alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                    else if (sdk >= 23)
                        alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                    else
                        alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                }
            }catch(Throwable e){
                Log.e("ProfileServiceLoadProf", Log.getStackTraceString(e));
            }
        }
        final void setAPM(boolean enable) {
            try {
                boolean on;
                if(Build.VERSION.SDK_INT >= 17)
                    on = Settings.Global.getInt(mContext.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
                else
                    on = Settings.System.getInt(mContext.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1;
                if(on != enable) {
                    if(Build.VERSION.SDK_INT > 16) {
                        final int toggle = (enable) ? 1 : 0;
                        final Process su = Runtime.getRuntime().exec("su");
                        final DataOutputStream output = new DataOutputStream(su.getOutputStream());
                        output.writeBytes("settings put global airplane_mode_on " + toggle + "\n");
                        output.flush();
                        output.writeBytes("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state " + toggle + "\n");
                        output.flush();
                        output.writeBytes("exit\n");
                        output.flush();
                        su.waitFor();
                    } else {
                        Settings.System.putInt(getContentResolver(), Settings.System.AIRPLANE_MODE_ON, on ? 0 : 1);
                        // Post an intent to reload.
                        Intent reload = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
                        reload.putExtra("state", !on);
                        sendBroadcast(reload);
                    }
                }
            }catch(Throwable e) {
                Log.e("ProfileServiceTogAPM", Log.getStackTraceString(e));
            }
        }
        final void setMobileData(Context context, boolean value) {
            //works perfecrly on 4.4.2 Dalvik MIUI
            boolean enabled;
            if(Build.VERSION.SDK_INT >= 17) {
                enabled = Settings.Global.getInt(context.getContentResolver(), "mobile_data", 1) == 1;
            } else {
                enabled = Settings.Secure.getInt(context.getContentResolver(), "mobile_data", 1) == 1;
            }
            if(enabled != value) {
                try {
                    if (Build.VERSION.SDK_INT > 19) {
                        final Process su = Runtime.getRuntime().exec("su");
                        final DataOutputStream output = new DataOutputStream(su.getOutputStream());
                        if (value)
                            output.writeBytes("svc data enable\n");
                        else
                            output.writeBytes("svc data disable\n");
                        output.flush();
                        //output.writeBytes("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state " + toggle + "\n");
                        //output.flush();
                        output.writeBytes("exit\n");
                        output.flush();
                        su.waitFor();
                    } else {
                        final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                        //final Field iConnectivityManagerField = Class.forName(conman.getClass().getName()).getDeclaredField("mService");
                        //iConnectivityManagerField.setAccessible(true);
                        //final Object iConnectivityManager = iConnectivityManagerField.get(conman);
                        final Method setMobileDataMethod = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
                        setMobileDataMethod.setAccessible(true);
                        setMobileDataMethod.invoke(conman, value);
                    }
                } catch(Throwable e) {
                    Log.e("ProfileService", Log.getStackTraceString(e));
                }
            }
        }
        final void setGPS(boolean toggle) {
            try {
                if(Build.VERSION.SDK_INT >= 17) {
                    //TODO: Implement check before we run code
                    final Process su = Runtime.getRuntime().exec("su");
                    final DataOutputStream output = new DataOutputStream(su.getOutputStream());
                    if (toggle)
                        output.writeBytes("settings put secure location_providers_allowed gps,network\n");
                    else
                        output.writeBytes("settings put secure location_providers_allowed network\n");
                    output.flush();
                    //output.writeBytes("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state " + toggle + "\n");
                    //output.flush();
                    output.writeBytes("exit\n");
                    output.flush();
                    su.waitFor();
                } else {
                    final Intent gps = new Intent();
                    gps.setComponent(new ComponentName("com.github.haoyangw.gpstoggle", "com.github.haoyangw.gpstoggle.ExecService"));
                    gps.putExtra("purpose", "Exec Code GPS");
                    gps.putExtra("toggle", toggle);
                    mContext.startService(gps);
                }
            }catch(Throwable e) {
                Log.e("ProfileService", Log.getStackTraceString(e));
            }
        }
        final void setGPS(int mode) {
            try {
                final Intent gps = new Intent();
                gps.setComponent(new ComponentName("com.github.haoyangw.gpstoggle", "com.github.haoyangw.gpstoggle.ExecService"));
                gps.putExtra("purpose", "Exec Code GPS");
                gps.putExtra("mode", mode);
                mContext.startService(gps);
                /**final Process su = Runtime.getRuntime().exec("su");
                final DataOutputStream output = new DataOutputStream(su.getOutputStream());
                output.writeBytes("settings put secure location_mode " + String.valueOf(mode) +"\n");
                output.flush();
                //output.writeBytes("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state " + toggle + "\n");
                //output.flush();
                output.writeBytes("exit\n");
                output.flush();
                su.waitFor();**/
            }catch(Throwable e) {
                Log.e("ProfileService", Log.getStackTraceString(e));
            }
        }
        final void setBT(boolean enable) {
            try{
                if(Build.VERSION.SDK_INT <= 15)
                    Looper.prepare();
                BluetoothAdapter bt = BluetoothAdapter.getDefaultAdapter();
                if(bt.isEnabled() && !(enable)) {
                    bt.disable();
                } else if(!(bt.isEnabled()) && enable) {
                    bt.enable();
                }
            }catch(Throwable e) {
                Log.e("ProfileServicesetBT", Log.getStackTraceString(e));
            }
        }
    }
}
