package com.github.haoyangw.simpleprofiles;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfilesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfilesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ProfileData mListener;

    private String profiles = "";
    private String profList[] = null;
    boolean[] selected = null;
    private ProfilesAdapter mAdapter;
    private RadioButton prevV = null; //Previously selected profile's radiobutton
    private String what = "";
    private boolean modded = false; //Whether change in number of profiles
    RecyclerView mList;
    private int prev = -1;
    private Context mContext;
    private boolean root = true;

    public ProfilesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfilesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfilesFragment newInstance(String param1, String param2) {
        ProfilesFragment fragment = new ProfilesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        final File su = new File("/system/xbin/su");
        if(!su.exists()) {
            root = false;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.stop_boot).setVisible(false);
        menu.findItem(R.id.debug_al).setVisible(false);
        menu.findItem(R.id.rem_al).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_clear) {
            //TODO: Implement clear all
            clearAll();
            return true;
        }
        else if(id == R.id.action_about) {
            return false;
        }
        else if(id == R.id.action_help) {
            return false;
        }
        else if (id == R.id.action_settings) {
            return false;
        } else if(id == R.id.stop_boot) {
            return false;
        }
        else if (id == R.id.debug_al) {
            return false;
        } else if(id == R.id.rem_al) {
            return false;
        } else if(id == R.id.action_exit) {
            return false;
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_profiles, container, false);
        init(false);
        mList = (RecyclerView)rootView.findViewById(R.id.list);
        mAdapter = new ProfilesAdapter();

        if (mList != null) {
            mList.setAdapter(mAdapter);
            mList.setLayoutManager(new LinearLayoutManager(getActivity()));
            mList.setHasFixedSize(true);
        }
        FloatingActionButton fab = (FloatingActionButton)rootView.findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickAdd(view);
                    //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    //.setAction("Action", null).show();
                }
            });
        }
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mListener = (ProfileData)context;
        /**if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }**/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        //final Activity mActivity = (Activity)mContext;
    }

    @Override
    public void onStart() {
        super.onStart();
        final ActionBar act = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (!act.isShowing())
            act.show();
        act.setTitle(R.string.app_name);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface ProfileData {
        // TODO: Update argument type and name
        void passName(String profile, boolean isConfig, boolean isRoot);
    }

    public final class ProfilesAdapter extends RecyclerView.Adapter<ProfilesAdapter.ProfilesHolder> {
        @Override
        public ProfilesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View row = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.proflist, parent, false);
            return new ProfilesHolder(row);
        }

        @Override
        public void onBindViewHolder(ProfilesHolder holder, int position) {
            holder.name.setText(profList[position]);
            holder.config.setOnClickListener(new ProfilesAdapter.mConfigListener(position));
            if (selected != null) {
                if(selected.length != 0) {
                    if (position < selected.length) {
                        holder.select.setChecked(selected[position]);
                        holder.select.setOnClickListener(new ProfilesAdapter.mSelectListener(position));
                        //if (selected[position]) {
                        //prev = position;
                        //}
                    }
                }
            }
        }

        @Override
        public int getItemCount() {
            return profList.length;
        }

        final class ProfilesHolder extends RecyclerView.ViewHolder {
            RadioButton select;
            public TextView name;
            Button config;
            ProfilesHolder(View v) {
                super(v);
                name = (TextView)v.findViewById(R.id.name);
                select = (RadioButton)v.findViewById(R.id.select);
                config = (Button)v.findViewById(R.id.config);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onListItemClick(getAdapterPosition());
                    }
                });
                v.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        return onListItemLongClick(getAdapterPosition());
                    }
                });
            }
        }
        private class mSelectListener implements View.OnClickListener {
            private int position;
            mSelectListener(int pos) {
                position = pos;
            }
            @Override
            public void onClick(View view) {
                try {
                    //final RecyclerView lv = (RecyclerView)view.getParent().getParent();
                    //final int position = lv.getPositionForView(view);
                    final RadioButton select = (RadioButton) view;
                    if (prevV != null && !modded) { //TODO: Reduce checking required to secure singleChoice
                        //if(lv.getPositionForView(prevV) == prev) {
                        prevV.setChecked(false);
                        //final int prevPos = lv.getPositionForView(prevV);
                        selected[prev] = false;
                        selected[position] = true;
                        select.setChecked(true);//(selected[position]);
                        /**} else {
                         Arrays.fill(selected, false);
                         selected[position] = true;
                         }**/
                    } else{
                        Arrays.fill(selected, false);
                        selected[position] = true;
                    }
                    final StringBuilder sb = new StringBuilder();
                    final int len = selected.length;
                    for (int i = 0; i < len; i++) {
                        sb.append(String.valueOf(selected[i]));
                        if (i != selected.length - 1) {
                            sb.append(",");
                        }
                    }
                    if (prevV == null || modded) {//|| lv.getPositionForView(prevV) != prev) {
                        mAdapter.notifyDataSetChanged();
                        modded = false;
                    }
                    prevV = select;
                    prev = position; //TODO: Reduce the amount of fields required to secure singleChoice?
                    final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                    editor.putString("selected", sb.toString());
                    editor.apply();
                    //final Intent loadProf = new Intent("com.why.loadProf");
                    final Intent loadProf = new Intent(mContext, ProfileService.class);
                    loadProf.putExtra("name", profList[position]);
                    loadProf.putExtra("bg", false);
                    mContext.startService(loadProf);
                } catch(Throwable e) {
                    Log.e("ProfileSelect", Log.getStackTraceString(e));
                }
            }
        }

        private class mConfigListener implements View.OnClickListener {
            private int position;
            mConfigListener(int pos) {
                position = pos;
            }
            @Override
            public void onClick(View view) {
                final String profName = profList[position];
                if(!profName.equals("")) {
                    modded = true;
                    mListener.passName(profName, true, root);
                    /**final Intent config = new Intent(mContext, Config.class);
                    config.putExtra("name", profName);
                    mContext.startActivity(config);**/
                }
            }
        }
    }

    public final class save implements Runnable {
        private Context mContext;
        private boolean remove;

        save(Context context, boolean rem) {
            mContext = context;
            remove = rem;
        }

        @SuppressLint("CommitPrefEdits")
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            try {
                final StringBuilder sb = new StringBuilder();
                final StringBuilder sb2 = new StringBuilder();
                if(!remove) {
                    if (!what.equals("")) {
                        if (profiles.equals("")) {
                            profiles = what;
                            sb.append(profiles);
                        } else {// (!profiles.equals("")) //has other profiles i.e. profiles = "Home,Home,Home"
                            sb.append(profiles).append(",").append(what);
                        }
                        what = "";
                        profiles = sb.toString();
                    }
                    profList = profiles.split(",");
                    //Log.i("Configs", profList[profList.length-1]);
                } else {
                    final int len = profList.length;
                    for(int i = 0; i < len; i++) {
                        sb.append(profList[i]);
                        if(i != profList.length-1) {
                            sb.append(",");
                        }
                    }
                    profiles = sb.toString();
                }
                for (int i = 0; i < selected.length; i++) {
                    sb2.append(String.valueOf(selected[i]));
                    if (i != selected.length - 1) {
                        sb2.append(",");
                    }
                }
                final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                editor.putString("pref_profiles", profiles);
                editor.putString("selected", sb2.toString());
                editor.commit();
            } catch (Throwable e) {
                Log.e("Profilesave", Log.getStackTraceString(e));
            }
        }
    }

    public final class saveArr implements Runnable
    {
        private Context mContext;
        private int position;
        private String nm;

        saveArr(Context context, int pos, String name) {
            mContext = context;
            position = pos;
            nm = name;
        }

        @SuppressLint("CommitPrefEdits")
        public void run()
        {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            try {
                if(profList != null) {
                    profList[position] = nm;
                    final StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < profList.length; i++) {
                        sb.append(profList[i]);
                        if (i != profList.length - 1) {
                            sb.append(",");
                        }
                    }
                    final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                    editor.putString("pref_profiles", sb.toString());
                    editor.commit();
                }
                else
                    Log.e("ProfilesSvArr", "Profile list is null");
            } catch (Throwable e) {
                Log.e("ProfilesSvArr", Log.getStackTraceString(e));
            }
        }
    }

    private void onListItemClick(int position) {
        final int pos = position;
        final EditText input = new EditText(mContext);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint(R.string.rename);
        AlertDialog.Builder renm = new AlertDialog.Builder(mContext);
        renm.setTitle("Rename Profile")
                .setView(input)
                .setCancelable(false)
                .setPositiveButton("Rename", new DialogInterface.OnClickListener() {
                    @SuppressLint("SdCardPath")
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            final String newnam = input.getText().toString();
                            if(!newnam.equals("")) {
                                final String check = newnam.toLowerCase();
                                List<String> list = Arrays.asList(profList);
                                boolean pass = true;
                                for(String s:list) {
                                    if(pass)
                                        pass = !s.equalsIgnoreCase(check);
                                }
                                if(pass) {
                                    boolean success = true;
                                    final String oldnam = profList[pos];
                                    final File conf = new File("/data/data/" + mContext.getPackageName() + "/shared_prefs/" + oldnam + ".xml");
                                    if (conf.exists()) {
                                        success = conf.renameTo(new File("/data/data/" + mContext.getPackageName() + "/shared_prefs/" + newnam + ".xml"));
                                    }
                                    Thread rename = new Thread(new saveArr(mContext, pos, newnam));
                                    rename.start();
                                    rename.join();
                                    mAdapter.notifyDataSetChanged();
                                    if(!success)
                                        Log.e("ProfilesRename", "One profile preference failed to be renamed");
                                } else
                                    Toast.makeText(mContext, "A profile with the name " + newnam + " already exists", Toast.LENGTH_LONG).show();
                            } else
                                Log.e("ProfilesRn", "Profile not given new name to rename to");
                        }catch(Throwable e) {
                            Log.e("ProfilesRn", Log.getStackTraceString(e));
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }

    protected final boolean onListItemLongClick(int pos) {
        final int position = pos;
        AlertDialog.Builder clear = new AlertDialog.Builder(mContext);
        clear.setTitle("Delete profile")
                .setMessage("Are you sure you wish to delete profile " + profList[pos] + "?")
                .setCancelable(false)
                .setPositiveButton("Yes, delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
                        final String conf = profList[position];
                        if(!selected[position]) {
                            try {
                                boolean success = true;
                                modded = true;
                                final List<String> profs = new LinkedList<>(Arrays.asList(profList));
                                profs.remove(profList[position]);
                                profList = profs.toArray(new String[profs.size()]);
                                boolean[] choices = new boolean[selected.length - 1];
                                for (int i = 0; i < choices.length; i++) {
                                    if (i != position && i < position) {
                                        choices[i] = selected[i];
                                    } else if(i >= position)
                                        choices[i] = selected[i+1];
                                }
                                selected = Arrays.copyOf(choices, choices.length);
                                File toDel = new File(mContext.getFilesDir().getParent() + "/shared_prefs/" + conf.toLowerCase() + ".xml");
                                if (toDel.exists())
                                    success = toDel.delete();
                                Thread saveData = new Thread(new save(mContext, true));
                                saveData.start();
                                saveData.join();
                                mAdapter.notifyDataSetChanged();
                                if(!success)
                                    Log.e("ProfilesRemove", "One profile preference failed to be deleted");
                            } catch (Throwable e) {
                                Log.e("ProfilesRemove", Log.getStackTraceString(e));
                            }
                        } else if(selected[position]) {
                            Toast.makeText(mContext, "Profile '" + conf + "' is enabled! Disable it first", Toast.LENGTH_LONG).show();
                            Log.e("ProfilesRemove", "Profile is enabled");
                        }
                    }
                })
                .setNegativeButton("No, cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
        return true;
    }

    public final void clearAll() {
        AlertDialog.Builder clear = new AlertDialog.Builder(mContext);
        clear.setTitle("Clear all Profiles")
                .setMessage("Are you sure you wish to clear all profiles? This canNOT be undone!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        boolean success = true;
                        for (String prof : profList) {
                            File toDel = new File(mContext.getFilesDir().getParent() + "/shared_prefs/" + prof.toLowerCase() + ".xml");
                            if (toDel.exists())
                                success = toDel.delete();
                        }
                        profiles = "";
                        profList = new String[0];
                        selected = new boolean[0];
                        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(mContext);
                        final String theme = shared.getString("pref_theme", "dark");
                        final boolean first = shared.getBoolean("first_run", false);
                        final boolean clean = shared.getBoolean("first_time", false);
                        final SharedPreferences.Editor editor = shared.edit();
                        editor.clear();
                        editor.putString("pref_theme", theme);
                        editor.putBoolean("first_run", first);
                        editor.putBoolean("first_time", clean);
                        //editor.putString("pref_profiles", "");
                        //editor.putString("selected", "");
                        editor.apply();
                        load();//mAdapter.notifyDataSetChanged();
                        if(!success)
                            Log.e("ProfilesClearAll", "One or more profile preferences failed to be deleted");
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }

    public void onClickAdd(View view) {
        final EditText input = new EditText(mContext);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint(R.string.add);
        AlertDialog.Builder add = new AlertDialog.Builder(mContext);
        add.setTitle("New Profile")
                .setView(input)
                .setCancelable(false)
                .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            what = input.getText().toString();
                            if(!what.equals("")) {
                                final String lWhat = what.toLowerCase();
                                boolean pass = true;
                                if(profList != null) {
                                    if(profList.length != 0) {
                                        final List<String> list = Arrays.asList(profList);
                                        for (String string : list) {
                                            if (pass)
                                                pass = !string.equalsIgnoreCase(lWhat);
                                        }
                                    }
                                }
                                if(pass) {
                                    selected = Arrays.copyOf(selected, selected.length + 1);
                                    selected[selected.length-1] = false;
                                    Thread update = new Thread(new save(mContext, false));
                                    update.start();
                                    update.join();
                                    mAdapter.notifyDataSetChanged();
                                } else
                                    Toast.makeText(mContext, "A profile with the name '" + what + "' already exists", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(mContext, "New Profile not given a name", Toast.LENGTH_LONG).show();
                                Log.e("ProfilesAdd", "New Profile not given a name");
                            }
                        }catch(Throwable e) {
                            Log.e("ProfilesAdd", Log.getStackTraceString(e));
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }

    public final void init(boolean refresh) {
        try {
            //TODO: Possibly eliminate need for empty?
            boolean empty = false;
            profiles = PreferenceManager.getDefaultSharedPreferences(mContext).getString("pref_profiles", "");
            /**if((!profiles.equals("") && profList != null) || !refresh) {
             //if(profList.length != 0) {

             //}
             } else**/if(profiles.equals("")) {
                profiles="Home,Outdoor";
                empty = true;
                PreferenceManager.getDefaultSharedPreferences(mContext).edit()
                        .putString("pref_profiles", profiles).apply();
            }
            profList = profiles.split(",");
            selected = new boolean[profList.length];
            if(empty){
                Arrays.fill(selected, false);
            } else {
                final String sel = PreferenceManager.getDefaultSharedPreferences(mContext).getString("selected", "");
                if (!sel.equals("")) {
                    final String vals[] = sel.split(",");
                    for (int i = 0; i < selected.length; i++) {
                        selected[i] = Boolean.parseBoolean(vals[i]);
                    }
                }
            }
        }catch(Throwable e) {
            Log.e("ProfilesCreate", Log.getStackTraceString(e));
        }
        if(refresh)
            mAdapter.notifyDataSetChanged();
    }
    public final void load() {
        //TODO: Merge init and load? And watch performance?
        if(profiles.equals("")) {
            profiles="Home,Outdoor";
        }
        profList = profiles.split(",");
        selected = new boolean[profList.length];
        Arrays.fill(selected, false);
        mAdapter.notifyDataSetChanged();
    }

}
