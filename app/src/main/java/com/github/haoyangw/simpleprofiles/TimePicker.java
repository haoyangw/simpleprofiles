package com.github.haoyangw.simpleprofiles;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimePicker extends Fragment {
    static String prof = "";
    //static String trig = "";
    //static String[] trigs;
    static boolean isKK = true;
    static boolean isMM = false;
    private static AlarmManager alarms;
    private static PendingIntent load;
    private static Intent serv;
    //TODO: Reduce memory usage by reducing number of variables?
    String hourArr = "";
    static List<String> hr;
    String minArr = "";
    static List<String> mn;
    String ampmArr = "";
    static List<String> ampm;
    String enabledArr = "";
    static List<Boolean> enab;
    String repeatArr = "";
    static List<Boolean> rpt;
    String daysArr = "";
    static List<String> days;
    static List<String> selInd;
    @SuppressLint("StaticFieldLeak")
    static Context mContext;
    static boolean is24H = false;
    private static TriggersAdapter mAdapter;
    public static final List<String> order = Arrays.asList("Mon", "Tues", "Wed", "Thurs", "Fri", "Sat", "Sun");
    static boolean isFirst = false;

    public TimePicker() {
        // Required empty public constructor
    }

    public TimePicker newInstance(String profile) {
        final Bundle args = new Bundle();
        args.putString("name", profile);
        final TimePicker tp = new TimePicker();
        tp.setArguments(args);
        return tp;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /**trig = getActivity().getSharedPreferences(prof.toLowerCase(), Context.MODE_PRIVATE).getString("trigs", "");
        if(!trig.equals(""))
            trigs = trig.split(",");
        else
            trigs = new String[0];**/
        final SharedPreferences sp = getActivity().getSharedPreferences(prof.toLowerCase(), Context.MODE_PRIVATE);
        hourArr = sp.getString("hours", "");
        is24H = DateFormat.is24HourFormat(getActivity());
        if(!is24H) {
            ampmArr = sp.getString("ampms", "");
        }
        if(hourArr.equals("")) {
            /**hour = new int[0];
            min = new int[0];
            enabled = new boolean[0];
            repeat = new boolean[0];
            days = new String[0];
            selInd = new String[0];
            if(!is24H)
                ampm = new String[hour.length];**/
            hr = new ArrayList<>();
            mn = new ArrayList<>();
            ampm = new ArrayList<>();
            enab = new ArrayList<>();
            rpt = new ArrayList<>();
            days = new ArrayList<>();
            selInd = new ArrayList<>();
        } else {
            minArr = sp.getString("mins", "");
            enabledArr = sp.getString("enableds", "");
            repeatArr = sp.getString("repeats", "");
            daysArr = sp.getString("days", "");
            final String sIndArr = sp.getString("ind", "");
            String[] hourTmp = hourArr.split(",");
            hr = new ArrayList<>(Arrays.asList(hourTmp));
            String[] minTmp = minArr.split(",");
            mn = new ArrayList<>(Arrays.asList(minTmp));
            String[] enabTmp = enabledArr.split(",");
            enab = new ArrayList<>();
            for (String value : enabTmp) {
                enab.add(Boolean.parseBoolean(value));
            }
            String[] repeatTmp = repeatArr.split(",");
            rpt = new ArrayList<>();
            for(String value:repeatTmp){
                rpt.add(Boolean.parseBoolean(value));
            }
            days = new ArrayList<>(Arrays.asList(daysArr.split(";")));
            selInd = new ArrayList<>(Arrays.asList(sIndArr.split(";")));
            if(!is24H)
                ampm = new ArrayList<>(Arrays.asList(ampmArr.split(",")));
        }

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_time_picker, container, false);
        final FloatingActionButton fab = (FloatingActionButton)view.findViewById(R.id.add_trig);
        fab.setOnClickListener(new TimePicker.AddTriggerListener(getActivity()));
        final ListView lv = (ListView)view.findViewById(R.id.time_triggers);
        lv.setEmptyView(view.findViewById(R.id.trig_empty));
        mAdapter = new TriggersAdapter();
        lv.setAdapter(mAdapter);
        isFirst = PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean("first_time", true);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prof = getArguments().getString("name");
        alarms = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
        serv = new Intent(getActivity(), ProfileService.class);
        serv.putExtra("name", prof);
        serv.putExtra("bg", false);
        isKK = Build.VERSION.SDK_INT >= 19;
        isMM = Build.VERSION.SDK_INT >= 23;
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(menu != null) {
            menu.findItem(R.id.action_about).setVisible(false);
            menu.findItem(R.id.action_clear).setVisible(false);
            menu.findItem(R.id.action_help).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(false);
            menu.findItem(R.id.action_exit).setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        if(id == R.id.action_clear) {
            return false;
        }
        else if(id == R.id.action_about) {
            return false;
        }
        else if(id == R.id.action_help) {
            return false;
        }
        else if (id == R.id.action_settings) {
            return false;
        } else if(id == R.id.stop_boot) {
            final AlertDialog.Builder stop = new AlertDialog.Builder(mContext);
            stop.setTitle("Don't run on boot")
                    .setMessage("Are you sure you wish to disable the on boot receiver? Time triggers will not be fired!")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int id) {
                            final Thread disable = new Thread(new onBoot(false));
                            disable.start();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int id) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return true;
        } else if(id == R.id.debug_al) {
            final AlertDialog.Builder debug = new AlertDialog.Builder(mContext);
            final EditText input = new EditText(mContext);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setHint("Alarm pattern");
            debug.setTitle("Cancel alarm(Devs)")
                    .setView(input)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            final String patt = input.getText().toString();
                            final String[] ind = patt.split(",");
                            final int pos = Integer.parseInt(ind[0]);
                            final int val = Integer.parseInt(ind[1]);
                            serv.setData(Uri.parse("time:" + String.valueOf(pos*pos*val)));
                            load = PendingIntent.getService(mContext, pos*pos*val, serv, PendingIntent.FLAG_NO_CREATE);
                            alarms.cancel(load);
                            load.cancel();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return true;
        } else if(id == R.id.rem_al) {
            final int len = hr.size();
            final AlertDialog.Builder rem = new AlertDialog.Builder(mContext);
            rem.setTitle("Remove all time triggers")
                    .setMessage("Are you sure you wish to remove all time triggers? This canNOT be undone!")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            for(int i = 0; i < len; i++) {
                                cancelIt(i);
                            }
                            hr.clear();
                            mn.clear();
                            if(is24H)
                                ampm.clear();
                            enab.clear();
                            rpt.clear();
                            days.clear();
                            selInd.clear();
                            mAdapter.notifyDataSetChanged();
                            final Thread dec = new Thread(new declare(true));
                            dec.start();
                            final Thread save = new Thread(new saveDat(mContext, false, false));
                            save.start();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return true; //TODO: Add disable onboot receiver
        } else if(id == R.id.action_exit) {
            return false;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mContext = null;
    }

    public static void selDays(final View v, final int posi, final boolean rep) {
        final String[] dayArr = mContext.getResources().getStringArray(R.array.days);
        final TextView text = (TextView)(((View)v.getParent()).findViewById(R.id.days));
        final List<String> arr;
        final String daysAt;
        if(days.size() <= posi) {
            days.add(" ");
            daysAt = " ";
        } else {
            daysAt = days.get(posi);
        }
        final boolean[] selects = new boolean[dayArr.length];
        Arrays.fill(selects, false);
        if(selInd.size() <= posi)
            selInd.add(" ");
        final List<String> selArr;
        if(selInd.size() != 0 && !selInd.get(posi).equals(" ")) {
            final String[] nested = selInd.get(posi).split(",");
            selArr = new ArrayList<>(Arrays.asList(nested));
            for (String item : nested) {
                selects[Integer.parseInt(item)] = true;
            }
        } else {
            selArr = new ArrayList<>();
        }
        if((daysAt != null) && (!daysAt.equals("")) && !daysAt.equals(" ")) {
            arr = new ArrayList<>(Arrays.asList(daysAt.split(",")));
        } else {
            arr = new ArrayList<>();
        }

        AlertDialog.Builder daySel = new AlertDialog.Builder(mContext);
        daySel.setTitle(R.string.days_title)
              .setMultiChoiceItems(R.array.days, selects, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int index, boolean isChecked) {
                    final String selDay = dayArr[index];
                    if(isChecked) {
                        selects[index] = true;
                        arr.add(selDay);
                        selArr.add(String.valueOf(index));
                    } else {
                        arr.remove(selDay);
                        selArr.remove(String.valueOf(index));
                    }
                }
              })
              .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int which) {
                      final int len = arr.size();
                      final StringBuilder sb = new StringBuilder();
                      if(len != 0) {
                          Collections.sort(arr, comp);
                          for(int i = 0; i < len; i++) {
                              sb.append(arr.get(i));
                              if(i != len-1) {
                                  sb.append(",");
                              }
                          }
                      } else {
                          sb.append(" ");
                          ((CheckBox)v).setChecked(false);
                          text.setVisibility(View.GONE);
                      }
                      days.set(posi, sb.toString());
                      final int len1 = selArr.size();
                      final StringBuilder sb1 = new StringBuilder();
                      if(len1 != 0) {
                          for(int i = 0; i < len1; i++) {
                              sb1.append(selArr.get(i));
                              if(i != len1-1) {
                                  sb1.append(",");
                              }
                          }
                      } else {
                          sb1.append(" ");
                      }
                      cancelIt(posi);
                      if(rep)
                          rpt.set(posi, true);
                      selInd.set(posi, sb1.toString());
                      text.setText(days.get(posi));
                      final Thread fire = new Thread(new fireAlarm(posi));
                      fire.start();
                      final Thread saveIt = new Thread(new saveDat(mContext, true, rep));
                      saveIt.start();
                  }
              })
              .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                      Arrays.fill(selects, false);
                      if(rep) { //Was unchecked, gets checked, now cancel
                          ((CheckBox) v).setChecked(false);
                          rpt.set(posi, false);
                          text.setVisibility(View.GONE);
                      }
                      dialogInterface.dismiss();
                  }
              }).create().show();
    }

    private class AddTriggerListener implements View.OnClickListener {
        private Context context;
        AddTriggerListener(Context mContext) {
            context = mContext;
        }
        @Override
        public void onClick(View view) {
            final Picker picker = new Picker();
            DialogFragment timePicker = picker.newInstance(hr.size(), false);
            timePicker.show(((AppCompatActivity)context).getSupportFragmentManager(), "timePicker");
        }
    }

    private class TriggersAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return hr.size();
        }

        @Override
        public Object getItem(int pos) {
            return hr.get(pos); //???
        }

        @Override
        public long getItemId(int pos) {
            return pos;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            final int posi = pos;
            final TriggersHolder th;
            if(convertView == null) {
                convertView = LayoutInflater.from(mContext).
                        inflate(R.layout.time_row, parent, false);
                th = new TriggersHolder(convertView);
                convertView.setTag(th);
            } else {
                th = (TriggersHolder)convertView.getTag();
            }
            String hour = hr.get(pos);
            if(!is24H) { //TODO: Any perf improvements?
                int hourOD = Integer.parseInt(hour);
                if(hourOD > 12) {
                    hourOD = hourOD - 12;
                    hour = String.valueOf(hourOD);
                }
            }
            String min = mn.get(pos);
            if(Integer.parseInt(min) < 10)
                min = "0" + min;
            th.time.setText(hour + ":" + min);
            th.time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickTime(view, posi);
                }
            });
            if(!is24H)
                th.ap.setText(ampm.get(pos));
            else
                th.ap.setVisibility(View.GONE);
            th.toggle.setChecked(enab.get(pos));
            th.toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    onToggle(b, posi);
                }
            });
            th.repeat.setChecked(rpt.get(pos));
            th.repeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final boolean isChecked = ((CheckBox)view).isChecked();
                    onRepeat(view, isChecked, posi);
                }
            });
            if(!rpt.get(pos)) {
                th.dys.setVisibility(View.GONE);
                th.dys.setText("");
            } else {
                th.dys.setVisibility(View.VISIBLE);
                th.dys.setText(days.get(pos));
            }
            th.dys.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selDays(th.repeat, posi, false);
                }
            });
            th.del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onDelete(posi);
                }
            });
            return convertView;
        }
    }

    private class TriggersHolder {
        TextView time;
        TextView ap;
        SwitchCompat toggle;
        CheckBox repeat;
        TextView dys;
        ImageButton del;

        TriggersHolder(View view) {
            time = (TextView)view.findViewById(R.id.time);
            ap = (TextView)view.findViewById(R.id.ampm);
            toggle = (SwitchCompat)view.findViewById(R.id.trig_toggle);
            repeat = (CheckBox)view.findViewById(R.id.repeat);
            dys = (TextView)view.findViewById(R.id.days);
            del = (ImageButton)view.findViewById(R.id.delete);
        }
    }

    @SuppressLint("NewApi")
    public void onToggle(boolean isChecked, int posi) {
        //final CheckBox rep = (CheckBox)((View)view.getParent()).findViewById(R.id.repeat);
        //int id = (posi+1)*(posi+1);
        /**if(rep.isChecked()) {
            int len = selInd.get(posi).split(",").length;
            id = (posi+1) * (posi+1) * len;
        }
        load = PendingIntent.getService(mContext, id, serv, PendingIntent.FLAG_ONE_SHOT);**/
        if(!isChecked) {
            cancelIt(posi);
            enab.set(posi, false);
            if(!enab.contains(true)) {
                final Thread dec = new Thread(new declare(true));
                dec.start();
            }
        } else {
            enab.set(posi, true);
            final Thread fire = new Thread(new fireAlarm(posi));
            fire.start();
            /**final int hour = Integer.parseInt(hr.get(posi));
            final int min = Integer.parseInt(mn.get(posi));
            final Calendar c = Calendar.getInstance();
            c.setTimeInMillis(System.currentTimeMillis());
            c.set(Calendar.HOUR_OF_DAY, hour);
            c.set(Calendar.MINUTE, min);
            if (isKK && !isMM)
                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
            else if (isMM)
                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
            else
                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);**/
        }
        final Thread save = new Thread(new saveDat(mContext, false, false));
        save.start();
    }

    public void onClickTime(View view, int posi) {
        final Picker picker = new Picker();
        final CheckBox repeating = (CheckBox)((View)view.getParent().getParent().getParent()).findViewById(R.id.repeat);
        DialogFragment diag = picker.newInstance(posi, repeating.isChecked());
        diag.show(((AppCompatActivity)mContext).getSupportFragmentManager(), "timePicker");
    }

    public void onRepeat(View v, boolean isChecked, int pos) {
        final View view = ((View)v.getParent()).findViewById(R.id.days);
        if(isChecked) {
            view.setVisibility(View.VISIBLE);
            selDays(v, pos, true);
        } else {
            view.setVisibility(View.GONE);
            cancelIt(pos);
            rpt.set(pos, false);
            final Thread fire = new Thread(new fireAlarm(pos));
            fire.start();
            Thread save = new Thread(new saveDat(mContext, false, true));
            save.start();
        }
    }

    public void onDelete(int posi) {
        final int len = hr.size();
        if(posi != len-1) {
            for (int i = posi; i < len; i++) {
                if(enab.get(i))
                    cancelIt(i);
            }
        } else {
            if(enab.get(posi))
                cancelIt(posi);
        }
        hr.remove(posi);
        mn.remove(posi);
        ampm.remove(posi);
        enab.remove(posi);
        rpt.remove(posi);
        days.remove(posi);
        selInd.remove(posi);
        mAdapter.notifyDataSetChanged();
        if(posi != len-1) {
            for (int i = posi; i < len - 1; i++) {
                Thread fire = new Thread(new fireAlarm(i));
                fire.start();
            }
        }
        final Thread save = new Thread(new saveDat(mContext, false, false));
        save.start();
        if(posi == len-1) {
            try {
                save.join();
            } catch (InterruptedException e) {
                Log.e("ProfilesTime", Log.getStackTraceString(e));
            }
            final Thread dec = new Thread(new declare(true));
            dec.start();
        }
    }

    public static class saveDat implements Runnable {
        boolean alsoD = false;
        boolean alsoR = false;
        Context context;

        saveDat(Context con, boolean dy, boolean rpt) {
            context = con;
            alsoD = dy;
            alsoR = rpt;
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            try {
                final int len = rpt.size();
                final SharedPreferences.Editor edit = context.getSharedPreferences(prof.toLowerCase(), Context.MODE_PRIVATE).edit();
                if(alsoD) {
                    final StringBuilder dOW = new StringBuilder();
                    for (int i = 0; i < len; i++) {
                        String tmp = days.get(i);
                        //if(!tmp.equals(" ") && !tmp.equals("")) {
                            dOW.append(tmp);
                            if (i != len - 1) {
                                dOW.append(";");
                            }
                        //}
                    }
                    edit.putString("days", dOW.toString());
                    final StringBuilder sSB = new StringBuilder();
                    for(int i = 0; i < len; i++) {
                        String tmp = selInd.get(i);
                        //if(!tmp.equals(" ") && !tmp.equals("")) {
                            sSB.append(tmp);
                            if (i != len - 1) {
                                sSB.append(";");
                            }
                        //}
                    }
                    edit.putString("ind", sSB.toString());
                }
                if(alsoR) {
                    final StringBuilder rep = new StringBuilder();
                    for (int i = 0; i < len; i++) {
                        rep.append(rpt.get(i));
                        if (i != len - 1) {
                            rep.append(",");
                        }
                    }
                    edit.putString("repeats", rep.toString());
                } else if(!alsoD) {
                    int len1 = hr.size(); //TODO: add final?
                    final boolean isArr = len1 != 1;
                    final StringBuilder hSb = new StringBuilder();
                    for(int i = 0; i < len1; i++) {
                        hSb.append(hr.get(i));
                        if((i != len1-1) && (isArr))
                            hSb.append(",");
                    }
                    edit.putString("hours", hSb.toString());
                    final StringBuilder mSb = new StringBuilder();
                    for(int i = 0; i < len1; i++) {
                        mSb.append(mn.get(i));
                        if((i != len1-1)  && (isArr))
                            mSb.append(",");
                    }
                    edit.putString("mins", mSb.toString());
                    final StringBuilder eSb = new StringBuilder();
                    for(int i = 0; i < len1; i++) {
                        eSb.append(enab.get(i));
                        if((i != len1-1) && (isArr))
                            eSb.append(",");
                    }
                    edit.putString("enableds", eSb.toString());
                    final StringBuilder rSb = new StringBuilder();
                    int len2 = rpt.size();
                    for(int i = 0; i < len2; i++) {
                        rSb.append(rpt.get(i));
                        if((i != len2-1) && (isArr))
                            rSb.append(",");
                    }
                    edit.putString("repeats", rSb.toString());
                    int len3 = days.size();
                    final StringBuilder sISb = new StringBuilder();
                    for(int i = 0; i < len3; i++) {
                        sISb.append(selInd.get(i));
                        if((i != len3-1) && (isArr))
                            sISb.append(";");
                    }
                    edit.putString("ind", sISb.toString());
                    final StringBuilder dSb = new StringBuilder();
                    for(int i = 0; i < len3; i++) {
                        dSb.append(days.get(i));
                        if((i != len3-1) && (isArr))
                        dSb.append(";");
                    }
                    edit.putString("days", dSb.toString());
                    if(!is24H) {
                        final StringBuilder aSb = new StringBuilder();
                        for(int i = 0; i < len1; i++) {
                            aSb.append(ampm.get(i));
                            if((i != len1-1) && (isArr))
                                aSb.append(",");
                        }
                        edit.putString("ampms", aSb.toString());
                    }
                }
                edit.apply();
            }catch(Throwable e) {
                Log.e("ProfilesTime", Log.getStackTraceString(e));
            }
        }
    }

    public static class fireAlarm implements Runnable {
        int pos = -1;
        fireAlarm(int posi) {
            pos = posi;
        }

        @SuppressLint("NewApi")
        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            try {
                final int pos1 = pos + 1;
                final Calendar c = Calendar.getInstance();
                final Calendar now = Calendar.getInstance();
                c.setTimeInMillis(System.currentTimeMillis());
                c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hr.get(pos)));
                c.set(Calendar.MINUTE, Integer.parseInt(mn.get(pos)));
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                final boolean isRep = rpt.get(pos);
                final ArrayList<Integer> times = new ArrayList<>();
                times.add(Integer.valueOf(hr.get(pos)));
                times.add(Integer.valueOf(mn.get(pos)));
                serv.putExtra("time", times);
                if(isRep) {
                    //final int[] times = new int[2];
                    //times[0] = Integer.parseInt(hr.get(pos));
                    //times[1] = Integer.parseInt(mn.get(pos));
                    //Log.i("ProfilesTime", String.valueOf(times));
                    //serv.putExtra("time", times);
                    serv.putExtra("rep", true);
                    int id;
                    final String index = selInd.get(pos);
                    boolean ff = false;
                    if (!index.equals(" ") && !index.equals("")) {
                        if (index.contains("0")) {
                            id = pos1 * pos1;
                            c.set(Calendar.DAY_OF_WEEK, 2); //Mon
                            if (c.before(now)) {
                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                ff = true;
                            }
                            serv.putExtra("id", id);
                            serv.setData(Uri.parse("time:" + id));
                            load = PendingIntent.getService(mContext, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                            if (isKK && !isMM)
                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else if (isMM)
                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else
                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                        }
                        if (index.contains("1")) {
                            id = pos1 * pos1 * 2;
                            c.set(Calendar.DAY_OF_WEEK, 3); //Tues
                            if (c.before(now)) {
                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                if(!ff)
                                    ff = true;
                            } else if(ff) {
                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                if (c.before(now))
                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                            }
                            serv.putExtra("id", id);
                            serv.setData(Uri.parse("time:" + id));
                            load = PendingIntent.getService(mContext, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                            if (isKK && !isMM)
                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else if (isMM)
                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else
                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                        }
                        if (index.contains("2")) {
                            id = pos1 * pos1 * 3;
                            c.set(Calendar.DAY_OF_WEEK, 4); //Wed
                            if (c.before(now)) {
                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                if(!ff)
                                    ff = true;
                            } else if(ff) {
                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                if (c.before(now))
                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                            }
                            serv.putExtra("id", id);
                            serv.setData(Uri.parse("time:" + id));
                            load = PendingIntent.getService(mContext, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                            if (isKK && !isMM)
                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else if (isMM)
                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else
                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                        }
                        if (index.contains("3")) {
                            id = pos1 * pos1 * 4;
                            c.set(Calendar.DAY_OF_WEEK, 5); //Thurs
                            if (c.before(now)) {
                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                if(!ff)
                                    ff = true;
                            } else if(ff) {
                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                if (c.before(now))
                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                            }
                            serv.putExtra("id", id);
                            serv.setData(Uri.parse("time:" + id));
                            load = PendingIntent.getService(mContext, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                            if (isKK && !isMM)
                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else if (isMM)
                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else
                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                        }
                        if (index.contains("4")) {
                            id = pos1 * pos1 * 5;
                            c.set(Calendar.DAY_OF_WEEK, 6); //Fri
                            if (c.before(now)) {
                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                if(!ff)
                                    ff = true;
                            } else if(ff) {
                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                if (c.before(now))
                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                            }
                            serv.putExtra("id", id);
                            serv.setData(Uri.parse("time:" + id));
                            load = PendingIntent.getService(mContext, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                            if (isKK && !isMM)
                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else if (isMM)
                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else
                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                        }
                        if (index.contains("5")) {
                            id = pos1 * pos1 * 6;
                            c.set(Calendar.DAY_OF_WEEK, 7); //Sat
                            if (c.before(now)) {
                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                if(!ff)
                                    ff = true;
                            } else if(ff) {
                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                if (c.before(now))
                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                            }
                            serv.putExtra("id", id);
                            serv.setData(Uri.parse("time:" + id));
                            load = PendingIntent.getService(mContext, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                            if (isKK && !isMM)
                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else if (isMM)
                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else
                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                        }
                        if (index.contains("6")) {
                            id = pos1 * pos1 * 7;
                            c.set(Calendar.DAY_OF_WEEK, 1); //Sun
                            if (c.before(now)) {
                                c.add(Calendar.WEEK_OF_YEAR, 1);
                            } else if(ff) {
                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                if (c.before(now))
                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                            }
                            serv.putExtra("id", id);
                            serv.setData(Uri.parse("time:" + id));
                            load = PendingIntent.getService(mContext, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                            if (isKK && !isMM)
                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else if (isMM)
                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                            else
                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                        }
                    } else {
                        //TODO: Shouldn't happen?
                    }
                } else {
                    if(c.before(now)) {
                        c.add(Calendar.DAY_OF_WEEK, 1);
                    }
                    serv.putExtra("rep", false);
                    serv.setData(Uri.parse("time:" + String.valueOf(pos1*pos1)));
                    load = PendingIntent.getService(mContext, pos1 * pos1, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                    if (isKK && !isMM)
                        alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                    else if (isMM)
                        alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                    else
                        alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                }
            } catch(Throwable e) {
                Log.e("ProfilesTime", Log.getStackTraceString(e));
            }
        }
    }

    public static class onBoot implements Runnable {
        boolean state = true;

        onBoot(boolean toDo) {
            state = toDo;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            try {
                final ComponentName boot = new ComponentName(mContext, BootReceiver.class);
                final PackageManager pm = mContext.getPackageManager();
                if(state) {
                    pm.setComponentEnabledSetting(boot, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                            PackageManager.DONT_KILL_APP);
                } else {
                    pm.setComponentEnabledSetting(boot, PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                            PackageManager.DONT_KILL_APP);
                }
            }catch(Throwable e) {
                Log.e("ProfilesTime", Log.getStackTraceString(e));
            }
        }
    }

    public static class declare implements Runnable {
        boolean state = true;

        declare(boolean en) {
            state = en;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            try {
                mContext.getSharedPreferences(prof.toLowerCase(), Context.MODE_PRIVATE)
                        .edit().putBoolean("clean", state).apply();
            }catch(Throwable e) {
                Log.e("ProfilesTime", Log.getStackTraceString(e));
            }
        }
    }

    public static Comparator<String> comp = new Comparator<String>() {

        @Override
        public int compare(String first, String second) {
            return Integer.valueOf(order.indexOf(first))
                    .compareTo(order.indexOf(second));
        }
    };

    public static void cancelIt(int pos) {//TODO: Convert to thread
        try {
            final String index = selInd.get(pos);
            int pos1 = pos + 1;
            final boolean isRep = rpt.get(pos);
            if(isRep) {
                if (!index.equals(" ") && !index.equals("")) {
                    if (index.contains(",")) {
                        final String[] ind = index.split(",");
                        for (String value : ind) {
                            int val = Integer.parseInt(value) + 1;
                            serv.setData(Uri.parse("time:" + String.valueOf(pos1 * pos1 * val)));
                            load = PendingIntent.getService(mContext, pos1 * pos1 * val, serv, PendingIntent.FLAG_NO_CREATE);
                            if(load != null){
                                alarms.cancel(load);
                                load.cancel();
                            }
                        }
                    } else {
                        int val = Integer.parseInt(index) + 1;
                        serv.setData(Uri.parse("time:" + String.valueOf(pos1 * pos1 * val)));
                        load = PendingIntent.getService(mContext, pos1 * pos1 * val, serv, PendingIntent.FLAG_NO_CREATE);
                        if(load != null) {
                            alarms.cancel(load);
                            load.cancel();
                        }
                    }
                }
                else {
                    //Shouldn't happen
                }
            } else {
                serv.setData(Uri.parse("time:" + String.valueOf(pos1 * pos1)));
                load = PendingIntent.getService(mContext, pos1 * pos1, serv, PendingIntent.FLAG_NO_CREATE);
                if(load != null) {
                    alarms.cancel(load);
                    load.cancel();
                }
            }
        }catch(Throwable e) {
            Log.e("ProfilesTime", Log.getStackTraceString(e));
        }
    }

    public static class Picker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        final Calendar c = Calendar.getInstance();
        int pos = -1;
        boolean isRep = false;
        int callCount = 0;

        public Picker newInstance(int position, boolean repeating) {
            isRep = repeating;
            final Picker picker = new Picker();
            final Bundle args = new Bundle();
            args.putInt("position", position);
            picker.setArguments(args);
            return picker;
        }
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            pos = getArguments().getInt("position");
            //final Calendar c = Calendar.getInstance();
            //int hour;
            //if(is24H)
                final int hour = c.get(Calendar.HOUR_OF_DAY);//hour = c.get(Calendar.HOUR_OF_DAY);
            //else
            //    hour = c.get(Calendar.HOUR);
            final int min = c.get(Calendar.MINUTE);
            return new TimePickerDialog(
                    getActivity(), this, hour, min, is24H);
        }

        @SuppressLint("NewApi")
        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
            /**if(trig.equals("")) {
                trig = String.valueOf(trigs.length);
            } else {
                trig = trig + "," + String.valueOf(trigs.length);
            }
            trigs = trig.split(",");**/
            /**hour = Arrays.copyOf(hour, len+1);
            min = Arrays.copyOf(min, len+1);
            enabled = Arrays.copyOf(enabled, len+1);
            repeat = Arrays.copyOf(repeat, len+1);
            days = Arrays.copyOf(days, days.length+1);
            selInd = Arrays.copyOf(selInd, selInd.length+1);**/
            //final Calendar now = Calendar.getInstance();
            if(callCount == 0) {
                c.setTimeInMillis(System.currentTimeMillis());
                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                c.set(Calendar.MINUTE, minute);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                /**if(!isRep) {
                 int pos1 = pos + 1;
                 if(c.before(now)) {
                 c.add(Calendar.DAY_OF_WEEK, 1);
                 }
                 serv.setData(Uri.parse("time:" + String.valueOf(pos1*pos1)));
                 load = PendingIntent.getService(mContext, pos1 * pos1, serv, PendingIntent.FLAG_ONE_SHOT);
                 alarms.cancel(load);
                 if (isKK && !isMM)
                 alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                 else if(isMM)
                 alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                 else
                 alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                 } else {

                 }**/
                final int leng = hr.size();
                if (leng == 0) {
                    final Thread dec = new Thread(new declare(false));
                    dec.start();
                }
                final boolean notNew = leng > pos && leng != 0;
                final String hOD = String.valueOf(hourOfDay);
                final String min = String.valueOf(minute);
                if (notNew) {
                    hr.set(pos, hOD);
                    mn.set(pos, min);
                    if (!is24H) {
                        if (hourOfDay >= 12)
                            ampm.set(pos, "PM");
                        else
                            ampm.set(pos, "AM");
                    }
                } else {
                    hr.add(pos, hOD);
                    mn.add(pos, min);
                    enab.add(pos, true);
                    rpt.add(pos, false);
                    days.add(pos, " ");
                    selInd.add(pos, " ");
                    if (!is24H) {
                        if (hourOfDay >= 12)
                            ampm.add(pos, "PM");
                        else
                            ampm.add(pos, "AM");
                    }
                }
                cancelIt(pos);
                final Thread fire = new Thread(new fireAlarm(pos));
                fire.start();
                mAdapter.notifyDataSetChanged();
                final Thread save = new Thread(new saveDat(mContext, false, false));
                save.start();
                if (isFirst) {
                    PreferenceManager.getDefaultSharedPreferences(mContext).edit().putBoolean("first_time", false).apply();
                    final Thread enable = new Thread(new onBoot(true));
                    enable.start();
                    isFirst = false;
                    Log.i("Profiles", "Enabled OnBoot receiver to fire alarms"); //TODO: Hmm settings pref for allowing debug?
                }
            }
            callCount++;
            /**final SharedPreferences.Editor edit = getActivity().getSharedPreferences(prof.toLowerCase(), Context.MODE_PRIVATE).edit();
            int len = hr.size(); //TODO: add final?
            final boolean isArr = len != 1;
            final StringBuilder hSb = new StringBuilder();
            for(int i = 0; i < len; i++) {
                hSb.append(hr.get(i));
                if((i != len-1) && (isArr))
                    hSb.append(",");
            }
            edit.putString("hours", hSb.toString());
            final StringBuilder mSb = new StringBuilder();
            for(int i = 0; i < len; i++) {
                mSb.append(mn.get(i));
                if((i != len-1)  && (isArr))
                    mSb.append(",");
            }
            edit.putString("mins", mSb.toString());
            final StringBuilder eSb = new StringBuilder();
            for(int i = 0; i < len; i++) {
                eSb.append(enab.get(i));
                if((i != len-1) && (isArr))
                    eSb.append(",");
            }
            edit.putString("enableds", eSb.toString());
            final StringBuilder rSb = new StringBuilder();
            for(int i = 0; i < len; i++) {
                rSb.append(rpt.get(i));
                if((i != len-1) && (isArr))
                    rSb.append(",");
            }
            edit.putString("repeats", rSb.toString());
            final StringBuilder dSb = new StringBuilder();
            for(int i = 0; i < len; i++) {
                dSb.append(days[i]);
                if((i != days.length-1) && (isArr))
                    dSb.append(";");
            }
            edit.putString("days", dSb.toString());
            Thread save = new Thread(new saveDat(mContext, true, false));
            save.start();
            if(!is24H) {
                final StringBuilder aSb = new StringBuilder();
                for(int i = 0; i < len; i++) {
                    aSb.append(ampm.get(i));
                    if((i != len-1) && (isArr))
                        aSb.append(",");
                }
                edit.putString("ampms", aSb.toString());
            }
            edit.apply();**/
        }
    }

}
