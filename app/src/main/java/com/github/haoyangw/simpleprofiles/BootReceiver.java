package com.github.haoyangw.simpleprofiles;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @SuppressLint("NewApi")
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            try {
                final String profs = PreferenceManager.getDefaultSharedPreferences(context).getString("pref_profiles", "");
                final String[] profList = profs.split(",");
                final List<Boolean> clean = new ArrayList<>();
                for (String prof : profList) {
                    final SharedPreferences sp = context.getSharedPreferences(prof.toLowerCase(), Context.MODE_PRIVATE);
                    final boolean skip = sp.getBoolean("clean", true);
                    clean.add(skip);
                    if (!skip) {
                        final AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                        final Intent serv = new Intent(context, ProfileService.class);
                        serv.putExtra("name", prof);
                        serv.putExtra("bg", false);
                        PendingIntent load;
                        final boolean isKK = Build.VERSION.SDK_INT >= 19;
                        final boolean isMM = Build.VERSION.SDK_INT >= 23;
                        String hourArr = sp.getString("hours", "");
                        final String minArr = sp.getString("mins", "");
                        final String enabledArr = sp.getString("enableds", "");
                        final String repeatArr = sp.getString("repeats", "");
                        final String sIndArr = sp.getString("ind", "");
                        List<Integer> hr;
                        List<Integer> mn;
                        List<Boolean> enab;
                        List<Boolean> rpt;
                        List<String> selInd;
                        String[] hourTmp = hourArr.split(",");
                        hr = new ArrayList<>();
                        for (String value : hourTmp) {
                            hr.add(Integer.parseInt(value));
                        }
                        String[] minTmp = minArr.split(",");
                        mn = new ArrayList<>();
                        for (String value : minTmp) {
                            mn.add(Integer.parseInt(value));
                        }
                        String[] enabTmp = enabledArr.split(",");
                        enab = new ArrayList<>();
                        for (String value : enabTmp) {
                            enab.add(Boolean.parseBoolean(value));
                        }
                        String[] repeatTmp = repeatArr.split(",");
                        rpt = new ArrayList<>();
                        for (String value : repeatTmp) {
                            rpt.add(Boolean.parseBoolean(value));
                        }
                        selInd = new ArrayList<>(Arrays.asList(sIndArr.split(";")));
                        for (int pos = 0; pos < hr.size(); pos++) {
                            final boolean isEnab = enab.get(pos);
                            if (isEnab) {
                                final int pos1 = pos + 1;
                                final Calendar c = Calendar.getInstance();
                                final Calendar now = Calendar.getInstance();
                                c.setTimeInMillis(System.currentTimeMillis());
                                c.set(Calendar.HOUR_OF_DAY, hr.get(pos));
                                c.set(Calendar.MINUTE, mn.get(pos));
                                c.set(Calendar.SECOND, 0);
                                c.set(Calendar.MILLISECOND, 0);
                                final boolean isRep = rpt.get(pos);
                                final String text;
                                if (isRep) {
                                    final String index = selInd.get(pos);
                                    boolean ff = false;
                                    if (!index.equals(" ") && !index.equals("")) {
                                        int id = -1;
                                        final String times = hr.get(pos) + "," + mn.get(pos);
                                        serv.putExtra("rep", true);
                                        serv.putExtra("time", times);
                                        text = "': Repeat every " + index;
                                        if (index.contains("0")) {
                                            id = pos1 * pos1;
                                            c.set(Calendar.DAY_OF_WEEK, 2); //Mon
                                            if (c.before(now)) {
                                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                                ff = true;
                                            }
                                            serv.putExtra("id", id);
                                            serv.setData(Uri.parse("time:" + id));
                                            load = PendingIntent.getService(context, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                                            if (isKK && !isMM)
                                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else if (isMM)
                                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else
                                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                        }
                                        if (index.contains("1")) {
                                            id = pos1 * pos1 * 2;
                                            c.set(Calendar.DAY_OF_WEEK, 3); //Tues
                                            if (c.before(now)) {
                                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                                if(!ff)
                                                    ff = true;
                                            } else if(ff) {
                                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                                if (c.before(now))
                                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                                            }
                                            serv.putExtra("id", id);
                                            serv.setData(Uri.parse("time:" + id));
                                            load = PendingIntent.getService(context, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                                            if (isKK && !isMM)
                                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else if (isMM)
                                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else
                                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                        }
                                        if (index.contains("2")) {
                                            id = pos1 * pos1 * 3;
                                            c.set(Calendar.DAY_OF_WEEK, 4); //Wed
                                            if (c.before(now)) {
                                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                                if(!ff)
                                                    ff = true;
                                            } else if(ff) {
                                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                                if (c.before(now))
                                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                                            }
                                            serv.putExtra("id", id);
                                            serv.setData(Uri.parse("time:" + id));
                                            load = PendingIntent.getService(context, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                                            if (isKK && !isMM)
                                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else if (isMM)
                                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else
                                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                        }
                                        if (index.contains("3")) {
                                            id = pos1 * pos1 * 4;
                                            c.set(Calendar.DAY_OF_WEEK, 5); //Thurs
                                            if (c.before(now)) {
                                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                                if(!ff)
                                                    ff = true;
                                            } else if(ff) {
                                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                                if (c.before(now))
                                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                                            }
                                            serv.putExtra("id", id);
                                            serv.setData(Uri.parse("time:" + id));
                                            load = PendingIntent.getService(context, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                                            if (isKK && !isMM)
                                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else if (isMM)
                                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else
                                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                        }
                                        if (index.contains("4")) {
                                            id = pos1 * pos1 * 5;
                                            c.set(Calendar.DAY_OF_WEEK, 6); //Fri
                                            if (c.before(now)) {
                                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                                if(!ff)
                                                    ff = true;
                                            } else if(ff) {
                                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                                if (c.before(now))
                                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                                            }
                                            serv.putExtra("id", id);
                                            serv.setData(Uri.parse("time:" + id));
                                            load = PendingIntent.getService(context, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                                            if (isKK && !isMM)
                                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else if (isMM)
                                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else
                                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                        }
                                        if (index.contains("5")) {
                                            id = pos1 * pos1 * 6;
                                            c.set(Calendar.DAY_OF_WEEK, 7); //Sat
                                            if (c.before(now)) {
                                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                                if(!ff)
                                                    ff = true;
                                            } else if(ff) {
                                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                                if (c.before(now))
                                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                                            }
                                            serv.putExtra("id", id);
                                            serv.setData(Uri.parse("time:" + id));
                                            load = PendingIntent.getService(context, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                                            if (isKK && !isMM)
                                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else if (isMM)
                                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else
                                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                        }
                                        if (index.contains("6")) {
                                            id = pos1 * pos1 * 7;
                                            c.set(Calendar.DAY_OF_WEEK, 1); //Sun
                                            if (c.before(now)) {
                                                c.add(Calendar.WEEK_OF_YEAR, 1);
                                            } else if(ff) {
                                                c.add(Calendar.WEEK_OF_YEAR, -1);
                                                if (c.before(now))
                                                    c.add(Calendar.WEEK_OF_YEAR, 1);
                                            }
                                            serv.putExtra("id", id);
                                            serv.setData(Uri.parse("time:" + id));
                                            load = PendingIntent.getService(context, id, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                                            if (isKK && !isMM)
                                                alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else if (isMM)
                                                alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                            else
                                                alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                        }
                                    } else {
                                        text = "': Repeat every unknown days ERROR";
                                    }
                                } else {
                                    serv.putExtra("rep", false);
                                    text = "': One-time alarm";
                                    if (c.before(now)) {
                                        c.add(Calendar.DAY_OF_WEEK, 1);
                                    }
                                    serv.setData(Uri.parse("time:" + String.valueOf(pos1 * pos1)));
                                    load = PendingIntent.getService(context, pos1 * pos1, serv, PendingIntent.FLAG_CANCEL_CURRENT);
                                    if (isKK && !isMM)
                                        alarms.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                    else if (isMM)
                                        alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                    else
                                        alarms.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), load);
                                }
                                Log.i("ProfilesBoot", "'" + prof + text + " at " +
                                        hr.get(pos) + ":" + mn.get(pos));
                            }
                        }
                    } else {
                        Log.i("ProfilesBoot", "Skipped '" + prof + "' because there were no time triggers");
                    }
                }
                Toast.makeText(context, "Fired alarms for '" + profs + "'",
                        Toast.LENGTH_LONG).show(); //TODO: Read preferences to decide whether toast or noti
                Log.d("ProfilesBoot", "Fired alarms for " + profList.length + " profiles");
                if (!clean.contains(false)) {
                    final ComponentName boot = new ComponentName(context, BootReceiver.class);
                    final PackageManager pm = context.getPackageManager();
                    pm.setComponentEnabledSetting(boot, PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                            PackageManager.DONT_KILL_APP);
                    PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("first_time", true).apply();
                    Log.i("ProfilesBoot", "Disabled OnBoot Receiver because" +
                            " there are no active time triggers for any profile");
                }
            } catch(Throwable e) {
                Log.e("ProfilesBoot", Log.getStackTraceString(e));
            }
        }
    }
}
