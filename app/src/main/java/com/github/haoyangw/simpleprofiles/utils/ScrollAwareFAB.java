package com.github.haoyangw.simpleprofiles.utils;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by haoyang on 14/8/2016.
 */

public class ScrollAwareFAB extends FloatingActionButton.Behavior {
    public ScrollAwareFAB(Context context, AttributeSet attrs) {
        super();
    }
    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordlayout,
             FloatingActionButton fab, View directTagetChild, View target, int nestedScrollAxes) {
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL ||
                super.onStartNestedScroll(coordlayout, fab, directTagetChild, target, nestedScrollAxes);
    }
    @Override
    public void onNestedScroll(CoordinatorLayout coordlayout, FloatingActionButton fab,
             View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(coordlayout, fab, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
        if(dyConsumed > 0 && fab.getVisibility() == View.VISIBLE)
            fab.hide();
        else if(dyConsumed < 0 && fab.getVisibility() != View.VISIBLE)
            fab.show();
    }
}
