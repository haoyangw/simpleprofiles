package com.github.haoyangw.simpleprofiles;


import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class RootFragment extends Fragment {


    public RootFragment() {
        // Required empty public constructor
    }


    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_root, container, false);
        try {
            final TextView install = (TextView)view.findViewById(R.id.install_desc);
            if (install != null) {
                if (Build.VERSION.SDK_INT >= 19)
                    install.setText(getString(R.string.install_desc_kk) + getString(R.string.install_desc_end));
                else
                    install.setText(getString(R.string.install_desc) + getString(R.string.install_desc_end));
            }
        }catch (Throwable e) {
            Log.e("ProfilesRoot", Log.getStackTraceString(e));
        }
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.root_title);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(menu != null) {
            menu.findItem(R.id.action_about).setVisible(false);
            menu.findItem(R.id.action_clear).setVisible(false);
            menu.findItem(R.id.action_help).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(false);
            menu.findItem(R.id.stop_boot).setVisible(false);
            menu.findItem(R.id.debug_al).setVisible(false);
            menu.findItem(R.id.rem_al).setVisible(false);
            menu.findItem(R.id.action_exit).setVisible(false);
        }
    }

}
