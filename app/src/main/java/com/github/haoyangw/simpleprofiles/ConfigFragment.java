package com.github.haoyangw.simpleprofiles;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.github.haoyangw.simpleprofiles.utils.NetworkList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    private String profile = "";
    private boolean subscreen = false;
    private boolean root = true;

    public ConfigFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String rootKey) {
        //Log.i("Config", name);
        Bundle args = getArguments();
        profile = args.getString("profile");
        root = args.getBoolean("root");
        //final String key = args.getString(ARG_PREFERENCE_ROOT);
        /**if (key != null && key.equals("pref_trigger")) {
            subscreen = true;
            //((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        } else {
            final ActionBar act = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if(act != null) {
                if (!act.isShowing())
                    act.show();
            }
        }**/
        //TODO: Fix actionbar not showing after returning from nested preferencescreen
        //final ActionBar act = ((AppCompatActivity) getActivity()).getSupportActionBar();
        //if(act != null)
        //        act.hide();
        //if (!subscreen) {
            if (profile != null && !profile.equals("")) {
                final PreferenceManager prefMan = getPreferenceManager();
                prefMan.setSharedPreferencesName(profile.toLowerCase());
                //prefMan.setSharedPreferencesMode(Context.MODE_PRIVATE);
            }
            PreferenceManager.setDefaultValues(getActivity(), R.xml.profiles, false);
        //}
            setPreferencesFromResource(R.xml.profiles, rootKey);
        //if (!subscreen) {
            //addPreferencesFromResource(R.xml.profiles);
            if (profile != null && !profile.equals("")) {
                final boolean isEnabled = getActivity().getSharedPreferences(profile.toLowerCase(), Context.MODE_PRIVATE).getBoolean("pref_apm", false);
                if (isEnabled) {
                    getPreferenceManager().findPreference("pref_md").setEnabled(false);
                    getPreferenceManager().findPreference("pref_bt").setEnabled(false);
                }
                if (Build.VERSION.SDK_INT < 19)
                    getPreferenceScreen().removePreference(findPreference("pref_gps"));
                else
                    getPreferenceScreen().removePreference(findPreference("pref_gps_prekk"));
            }
        //}
        if(!root) {
            if(Build.VERSION.SDK_INT > 19)
                findPreference("pref_md").setVisible(false);
            findPreference("pref_gps").setVisible(false);
            findPreference("pref_gps_prekk").setVisible(false);
            if(Build.VERSION.SDK_INT > 16)
                findPreference("pref_apm").setVisible(false);
        }
        final Preference triggers = findPreference("pref_triggers");
        triggers.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                final TriggersFragment trigger = new TriggersFragment();
                final TriggersFragment trigg = trigger.newInstance(profile);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                        .replace(R.id.fragment_container, trigg)
                        .addToBackStack("triggers")
                        .commit();
                return true;
            }
        });
                /**final Preference wifi_trig = findPreference("wifi_trigger");
                wifi_trig.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        //if (preference.getKey().equals("pref_trigger")) {
                        final NetworkList nl = NetworkList.newInstance(profile);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                                .replace(R.id.fragment_container, nl)
                                .addToBackStack("wifi")
                                .commit();
                        //Intent wifi = new Intent("haoyangw.List");
                        //wifi.putExtra("name", profile);
                        //getActivity().startActivity(wifi);
                        return true;
                    }
                });
                final Preference time_trig = findPreference("time_trigger");
                time_trig.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        Toast.makeText(getActivity(), "To be implemented", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });**/
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(menu != null) {
            menu.findItem(R.id.action_about).setVisible(false);
            menu.findItem(R.id.action_clear).setVisible(false);
            menu.findItem(R.id.action_help).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(false);
            menu.findItem(R.id.stop_boot).setVisible(false);
            menu.findItem(R.id.debug_al).setVisible(false);
            menu.findItem(R.id.rem_al).setVisible(false);
            menu.findItem(R.id.action_exit).setVisible(false);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("'" + profile + "' configuration");
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        final boolean isEnabled = sharedPreferences.getBoolean("pref_apm", false);
        Log.i("Config", String.valueOf(isEnabled));
        if(key.equals("pref_apm")) {
            if(isEnabled) {
                ((CheckBoxPreference)findPreference("pref_md")).setChecked(false);
                ((CheckBoxPreference)findPreference("pref_bt")).setChecked(false);
            }
            //getPreferenceScreen().findPreference("pref_wifi").setEnabled(!isEnabled);
            findPreference("pref_md").setEnabled(!isEnabled);
            findPreference("pref_bt").setEnabled(!isEnabled);
        }
    }

    public static class TriggersFragment extends PreferenceFragmentCompat {
        private String profile = "";

        public TriggersFragment() {
        }

        public TriggersFragment newInstance(String profName) {
            TriggersFragment trig = new TriggersFragment();
            Bundle arg = new Bundle();
            arg.putString("profile", profName);
            trig.setArguments(arg);
            return trig;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @Override
        public void onStart() {
            super.onStart();
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Triggers for '" + profile + "'");
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            Bundle args = getArguments();
            profile = args.getString("profile");
            setPreferencesFromResource(R.xml.triggers, rootKey);
            final Preference wifi_trig = findPreference("wifi_trigger");
            wifi_trig.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    //if (preference.getKey().equals("pref_trigger")) {
                    final NetworkList nl = NetworkList.newInstance(profile);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                            .replace(R.id.fragment_container, nl)
                            .addToBackStack("wifi")
                            .commit();
                    //Intent wifi = new Intent("haoyangw.List");
                    //wifi.putExtra("name", profile);
                    //getActivity().startActivity(wifi);
                    return true;
                }
            });
            final Preference time_trig = findPreference("time_trigger");
            time_trig.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    //Toast.makeText(getActivity(), "To be implemented", Toast.LENGTH_SHORT).show();
                    final TimePicker tp = new TimePicker().newInstance(profile);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                            .replace(R.id.fragment_container, tp)
                            .addToBackStack("time")
                            .commit();
                    return true;
                }
            });
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            if(menu != null) {
                menu.findItem(R.id.action_about).setVisible(false);
                menu.findItem(R.id.action_clear).setVisible(false);
                menu.findItem(R.id.action_help).setVisible(false);
                menu.findItem(R.id.action_settings).setVisible(false);
                menu.findItem(R.id.stop_boot).setVisible(false);
                menu.findItem(R.id.debug_al).setVisible(false);
                menu.findItem(R.id.rem_al).setVisible(false);
                menu.findItem(R.id.action_exit).setVisible(false);
            }
        }
    }
}
