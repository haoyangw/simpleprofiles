package com.github.haoyangw.simpleprofiles;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionItemTarget;
import com.github.amlcurran.showcaseview.targets.ActionViewTarget;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class ProfilesActivity extends AppCompatActivity implements ProfilesFragment.ProfileData, PreferenceFragmentCompat.OnPreferenceStartScreenCallback {
    //private String profiles = "";
    //private String profList[] = null;
    //boolean[] selected = null;
    //private ProfilesAdapter mAdapter;
    //private RadioButton prevV = null; //Previously selected profile's radiobutton
    //private String what = "";
    //private boolean modded = false; //Whether change in number of profiles
    //RecyclerView mList;
    //private int prev = -1;
    int counter = 0;
    ShowcaseView scv;
    boolean first_run = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**try {
            profiles = PreferenceManager.getDefaultSharedPreferences(this).getString("pref_profiles", "");
            profList = profiles.split(",");
            final String sel = PreferenceManager.getDefaultSharedPreferences(this).getString("selected", "");
            selected = new boolean[profList.length];
            if (!sel.equals("")) {
                final String vals[] = sel.split(",");
                for (int i = 0; i < selected.length; i++) {
                    selected[i] = Boolean.parseBoolean(vals[i]);
                }
            }
        }catch(Throwable e) {
            Log.e("ProfilesCreate", Log.getStackTraceString(e));
        }**/
        //init(false);
        final SharedPreferences pref_theme = PreferenceManager.getDefaultSharedPreferences(this);
        final String theme = pref_theme.getString("pref_theme", "dark");
        switch (theme) {
            case "light":
                setTheme(R.style.AppThemeLightNoAB);
                break;
            case "lightab":
                setTheme(R.style.AppThemeLightNoAB);
                break;
            default:
                setTheme(R.style.AppThemeNoAB);
                break;
        }

        setContentView(R.layout.activity_profiles);
        /**mList = (RecyclerView)findViewById(R.id.list);
        mAdapter = new ProfilesAdapter();

        if (mList != null) {
            mList.setAdapter(mAdapter);
            mList.setLayoutManager(new LinearLayoutManager(this));
            mList.setHasFixedSize(true);
        }**/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new ProfilesFragment())
                .commit();
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /**FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickAdd(view);
                    //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            //.setAction("Action", null).show();
                }
            });
        }**/

        final String purpose = getIntent().getStringExtra("purpose");
        if(purpose != null) {
            if (purpose.equals("tutorial")) {
                //tut();
            }
        }
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                final int stackLength = getSupportFragmentManager().getBackStackEntryCount();
                final ActionBar ab = getSupportActionBar();
                if(ab != null) {
                    final boolean enabled = stackLength > 0;
                    ab.setDisplayHomeAsUpEnabled(enabled);
                    ab.setHomeButtonEnabled(enabled);
                }
            }
        });
        first_run = pref_theme.getBoolean("first_run", true);
        if(first_run) {
            //tut();
            //TODO: before the install system app thing, show a tutorial using showcaseview
            if(Build.VERSION.SDK_INT < 17 || Build.VERSION.SDK_INT >= 19) {
                //startActivity(new Intent(this, RootActivity.class));
                getSupportFragmentManager().beginTransaction()
                        .addToBackStack("root")
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                        .replace(R.id.fragment_container, new RootFragment())
                        .commit();
            }
            pref_theme.edit().putBoolean("first_run", false).apply();
        }
    }

    @Override
    protected void onResume() {
        //init(true);
        IntentFilter refresh = new IntentFilter();
        refresh.addAction("haoyangw.simpleprofiles.refresh");
        registerReceiver(UpdateList, refresh);
        super.onResume();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(UpdateList);
        super.onPause();
    }

    /**public final class ProfilesAdapter extends RecyclerView.Adapter<ProfilesAdapter.ProfilesHolder> {
        @Override
        public ProfilesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View row = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.proflist, parent, false);
            return new ProfilesHolder(row);
        }

        @Override
        public void onBindViewHolder(ProfilesHolder holder, int position) {
            holder.name.setText(profList[position]);
            holder.config.setOnClickListener(new mConfigListener(position));
            if (selected != null) {
                if(selected.length != 0) {
                    if (position < selected.length) {
                        holder.select.setChecked(selected[position]);
                        holder.select.setOnClickListener(new mSelectListener(position));
                        //if (selected[position]) {
                        //prev = position;
                        //}
                    }
                }
            }
        }

        @Override
        public int getItemCount() {
            return profList.length;
        }

        final class ProfilesHolder extends RecyclerView.ViewHolder {
            RadioButton select;
            public TextView name;
            Button config;
            ProfilesHolder(View v) {
                super(v);
                name = (TextView)v.findViewById(R.id.name);
                select = (RadioButton)v.findViewById(R.id.select);
                config = (Button)v.findViewById(R.id.config);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onListItemClick(getAdapterPosition());
                    }
                });
                v.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        return onListItemLongClick(getAdapterPosition());
                    }
                });
            }
        }
        private class mSelectListener implements View.OnClickListener {
            private int position;
            mSelectListener(int pos) {
                position = pos;
            }
            @Override
            public void onClick(View view) {
                try {
                    //final RecyclerView lv = (RecyclerView)view.getParent().getParent();
                    //final int position = lv.getPositionForView(view);
                    final RadioButton select = (RadioButton) view;
                    if (prevV != null && !modded) { //TODO: Reduce checking required to secure singleChoice
                        //if(lv.getPositionForView(prevV) == prev) {
                            prevV.setChecked(false);
                            //final int prevPos = lv.getPositionForView(prevV);
                            selected[prev] = false;
                            selected[position] = true;
                            select.setChecked(true);//(selected[position]);
                        } else {
                            Arrays.fill(selected, false);
                            selected[position] = true;
                        }
                    } else{
                        Arrays.fill(selected, false);
                        selected[position] = true;
                    }
                    final StringBuilder sb = new StringBuilder();
                    final int len = selected.length;
                    for (int i = 0; i < len; i++) {
                        sb.append(String.valueOf(selected[i]));
                        if (i != selected.length - 1) {
                            sb.append(",");
                        }
                    }
                    if (prevV == null || modded) {//|| lv.getPositionForView(prevV) != prev) {
                        mAdapter.notifyDataSetChanged();
                        modded = false;
                    }
                    prevV = select;
                    prev = position; //TODO: Reduce the amount of fields required to secure singleChoice?
                    final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                    editor.putString("selected", sb.toString());
                    editor.apply();
                    //final Intent loadProf = new Intent("com.why.loadProf");
                    final Intent loadProf = new Intent(getApplicationContext(), ProfileService.class);
                    loadProf.putExtra("name", profList[position]);
                    loadProf.putExtra("bg", false);
                    startService(loadProf);
                } catch(Throwable e) {
                    Log.e("ProfileSelect", Log.getStackTraceString(e));
                }
            }
        }

        private class mConfigListener implements View.OnClickListener {
            private int position;
            mConfigListener(int pos) {
                position = pos;
            }
            @Override
            public void onClick(View view) {
                final String profName = profList[position];
                if(!profName.equals("")) {
                    modded = true;
                    final Intent config = new Intent(getApplicationContext(), Config.class);
                    config.putExtra("name", profName);
                    startActivity(config);
                }
            }
        }
    }**/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profiles, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home) {
            getSupportFragmentManager().popBackStack();
            return true;
        }
        else if(id == R.id.action_clear) {
            //clearAll();
            //return true;
            return false;
        }
        else if(id == R.id.action_about) {
            //startActivity(new Intent(this, AboutActivity.class));
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack("about")
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                    .replace(R.id.fragment_container, new AboutFragment())
                    .commit();
            return true;
        }
        else if(id == R.id.action_help) {
            //TODO: Add ShowcaseView
            //startActivity(new Intent(this, HelpActivity.class));
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack("help")
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                    .replace(R.id.fragment_container, new HelpFragment())
                    .commit();
            return true;
        }
        else if (id == R.id.action_settings) {
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack("settings")
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                    .replace(R.id.fragment_container, new SettingsFragment())
                    .commit();
            //final Intent pref = new Intent(this, SettingsActivity.class);
            //startActivity(pref);
            return true;
        } else if (id == R.id.stop_boot) {
            return false;
        }
        else if (id == R.id.debug_al) {
            return false;
        } else if(id == R.id.action_exit) {
            Toast.makeText(this, "See you soon!", Toast.LENGTH_SHORT).show();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**public final class save implements Runnable {
        private Context mContext;
        private boolean remove;

        save(Context context, boolean rem) {
            mContext = context;
            remove = rem;
        }

        @SuppressLint("CommitPrefEdits")
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            try {
                final StringBuilder sb = new StringBuilder();
                final StringBuilder sb2 = new StringBuilder();
                if(!remove) {
                    if (!what.equals("")) {
                        if (profiles.equals("")) {
                            profiles = what;
                            sb.append(profiles);
                        } else {// (!profiles.equals("")) //has other profiles i.e. profiles = "Home,Home,Home"
                            sb.append(profiles).append(",").append(what);
                        }
                        what = "";
                        profiles = sb.toString();
                    }
                    profList = profiles.split(",");
                    //Log.i("Configs", profList[profList.length-1]);
                } else {
                    final int len = profList.length;
                    for(int i = 0; i < len; i++) {
                        sb.append(profList[i]);
                        if(i != profList.length-1) {
                            sb.append(",");
                        }
                    }
                    profiles = sb.toString();
                }
                for (int i = 0; i < selected.length; i++) {
                    sb2.append(String.valueOf(selected[i]));
                    if (i != selected.length - 1) {
                        sb2.append(",");
                    }
                }
                final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                editor.putString("pref_profiles", profiles);
                editor.putString("selected", sb2.toString());
                editor.commit();
            } catch (Throwable e) {
                Log.e("Profilesave", Log.getStackTraceString(e));
            }
        }
    }

    public final class saveArr implements Runnable
    {
        private Context mContext;
        private int position;
        private String nm;

        saveArr(Context context, int pos, String name) {
            mContext = context;
            position = pos;
            nm = name;
        }

        @SuppressLint("CommitPrefEdits")
        public void run()
        {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            try {
                if(profList != null) {
                    profList[position] = nm;
                    final StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < profList.length; i++) {
                        sb.append(profList[i]);
                        if (i != profList.length - 1) {
                            sb.append(",");
                        }
                    }
                    final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                    editor.putString("pref_profiles", sb.toString());
                    editor.commit();
                }
                else
                    Log.e("ProfilesSvArr", "Profile list is null");
            } catch (Throwable e) {
                Log.e("ProfilesSvArr", Log.getStackTraceString(e));
            }
        }
    }**/

    //@Override
    //private void onListItemClick(View view, int position, long id) {
    /**private void onListItemClick(int position) {
        final int pos = position;
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint(R.string.rename);
        AlertDialog.Builder renm = new AlertDialog.Builder(this);
        renm.setTitle("Rename Profile")
                .setView(input)
                .setCancelable(false)
                .setPositiveButton("Rename", new DialogInterface.OnClickListener() {
                    @SuppressLint("SdCardPath")
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            final String newnam = input.getText().toString();
                            if(!newnam.equals("")) {
                                final String check = newnam.toLowerCase();
                                List<String> list = Arrays.asList(profList);
                                boolean pass = true;
                                for(String s:list) {
                                    if(pass)
                                        pass = !s.equalsIgnoreCase(check);
                                }
                                if(pass) {
                                    boolean success = true;
                                    final String oldnam = profList[pos];
                                    final File conf = new File("/data/data/" + getPackageName() + "/shared_prefs/" + oldnam + ".xml");
                                    if (conf.exists()) {
                                        success = conf.renameTo(new File("/data/data/" + getPackageName() + "/shared_prefs/" + newnam + ".xml"));
                                    }
                                    Thread rename = new Thread(new saveArr(getApplicationContext(), pos, newnam));
                                    rename.start();
                                    rename.join();
                                    mAdapter.notifyDataSetChanged();
                                    if(!success)
                                        Log.e("ProfilesRename", "One profile preference failed to be renamed");
                                } else
                                    Toast.makeText(getApplicationContext(), "A profile with the name " + newnam + " already exists", Toast.LENGTH_LONG).show();
                            } else
                                Log.e("ProfilesRn", "Profile not given new name to rename to");
                        }catch(Throwable e) {
                            Log.e("ProfilesRn", Log.getStackTraceString(e));
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }

    protected final boolean onListItemLongClick(int pos) {
        final int position = pos;
        AlertDialog.Builder clear = new AlertDialog.Builder(this);
        clear.setTitle("Delete profile")
                .setMessage("Are you sure you wish to delete profile " + profList[pos] + "?")
                .setCancelable(false)
                .setPositiveButton("Yes, delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
                        final String conf = profList[position];
                        if(!selected[position]) {
                            try {
                                boolean success = true;
                                modded = true;
                                final List<String> profs = new LinkedList<>(Arrays.asList(profList));
                                profs.remove(profList[position]);
                                profList = profs.toArray(new String[profs.size()]);
                                boolean[] choices = new boolean[selected.length - 1];
                                for (int i = 0; i < choices.length; i++) {
                                    if (i != position && i < position) {
                                        choices[i] = selected[i];
                                    } else if(i >= position)
                                        choices[i] = selected[i+1];
                                }
                                selected = Arrays.copyOf(choices, choices.length);
                                File toDel = new File(getFilesDir().getParent() + "/shared_prefs/" + conf.toLowerCase() + ".xml");
                                if (toDel.exists())
                                    success = toDel.delete();
                                Thread saveData = new Thread(new save(getApplicationContext(), true));
                                saveData.start();
                                saveData.join();
                                mAdapter.notifyDataSetChanged();
                                if(!success)
                                    Log.e("ProfilesRemove", "One profile preference failed to be deleted");
                            } catch (Throwable e) {
                                Log.e("ProfilesRemove", Log.getStackTraceString(e));
                            }
                        } else if(selected[position]) {
                            Toast.makeText(getApplicationContext(), "Profile '" + conf + "' is enabled! Disable it first", Toast.LENGTH_LONG).show();
                            Log.e("ProfilesRemove", "Profile is enabled");
                        }
                    }
                })
                .setNegativeButton("No, cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
        return true;
    }

    public void onClickAdd(View view) {
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint(R.string.add);
        AlertDialog.Builder add = new AlertDialog.Builder(this);
        add.setTitle("New Profile")
                .setView(input)
                .setCancelable(false)
                .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            what = input.getText().toString();
                            if(!what.equals("")) {
                                final String lWhat = what.toLowerCase();
                                boolean pass = true;
                                if(profList != null) {
                                    if(profList.length != 0) {
                                        final List<String> list = Arrays.asList(profList);
                                        for (String string : list) {
                                            if (pass)
                                                pass = !string.equalsIgnoreCase(lWhat);
                                        }
                                    }
                                }
                                if(pass) {
                                    selected = Arrays.copyOf(selected, selected.length + 1);
                                    selected[selected.length-1] = false;
                                    Thread update = new Thread(new save(getApplicationContext(), false));
                                    update.start();
                                    update.join();
                                    mAdapter.notifyDataSetChanged();
                                } else
                                    Toast.makeText(getApplicationContext(), "A profile with the name '" + what + "' already exists", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "New Profile not given a name", Toast.LENGTH_LONG).show();
                                Log.e("ProfilesAdd", "New Profile not given a name");
                            }
                        }catch(Throwable e) {
                            Log.e("ProfilesAdd", Log.getStackTraceString(e));
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }

    public final void clearAll() {
        AlertDialog.Builder clear = new AlertDialog.Builder(this);
        clear.setTitle("Clear all Profiles")
                .setMessage("Are you sure you wish to clear all profiles? This canNOT be undone!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        boolean success = true;
                        for (String prof : profList) {
                            File toDel = new File(getFilesDir().getParent() + "/shared_prefs/" + prof.toLowerCase() + ".xml");
                            if (toDel.exists())
                                success = toDel.delete();
                        }
                        profiles = "";
                        profList = new String[0];
                        selected = new boolean[0];
                        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        final String theme = shared.getString("pref_theme", "dark");
                        final SharedPreferences.Editor editor = shared.edit();
                        editor.clear();
                        editor.putString("pref_theme", theme);
                        //editor.putString("pref_profiles", "");
                        //editor.putString("selected", "");
                        editor.apply();
                        load();//mAdapter.notifyDataSetChanged();
                        if(!success)
                            Log.e("ProfilesClearAll", "One or more profile preferences failed to be deleted");
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }
    public final void init(boolean refresh) {
        try {
            //TODO: Possibly eliminate need for empty?
            boolean empty = false;
            profiles = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("pref_profiles", "");
                //if((!profiles.equals("") && profList != null) || !refresh) {
                //if(profList.length != 0) {

                //}
            //} else
                if(profiles.equals("")) {
                profiles="Home,Outdoor";
                empty = true;
            }
            profList = profiles.split(",");
            selected = new boolean[profList.length];
            if(empty){
                Arrays.fill(selected, false);
            } else {
                final String sel = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("selected", "");
                if (!sel.equals("")) {
                    final String vals[] = sel.split(",");
                    for (int i = 0; i < selected.length; i++) {
                        selected[i] = Boolean.parseBoolean(vals[i]);
                    }
                }
            }
        }catch(Throwable e) {
            Log.e("ProfilesCreate", Log.getStackTraceString(e));
        }
        if(refresh)
            mAdapter.notifyDataSetChanged();
    }
    public final void load() {
        //TODO: Merge init and load? And watch performance?
        if(profiles.equals("")) {
            profiles="Home,Outdoor";
        }
        profList = profiles.split(",");
        selected = new boolean[profList.length];
        Arrays.fill(selected, false);
        mAdapter.notifyDataSetChanged();
    }**/

    public void onClickLaunchInstall(View view) {
        getSupportFragmentManager().beginTransaction()
                .addToBackStack("root")
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                .replace(R.id.fragment_container, new RootFragment())
                .commit();
    }

    public void onClickInstall(View view) {
        try {
            final AssetManager assets = getAssets();
            InputStream in = assets.open("GPSToggle.apk");
            final String target = getFilesDir().getPath() + "/GPSToggle.apk";
            final File file = new File(target);
            if(!file.exists()) {
                final OutputStream write = new FileOutputStream(target);
                final byte[] buffer = new byte[1024];
                int read;
                while((read = in.read(buffer)) != -1) {
                    write.write(buffer, 0, read);
                }
                in.close();
                write.flush();
                write.close();
            }
            final Process root = Runtime.getRuntime().exec("su");
            final DataOutputStream output = new DataOutputStream(root.getOutputStream());
            output.writeBytes("mount -o remount, rw /system\n");
            output.flush();
            if(Build.VERSION.SDK_INT < 19) {
                if (new File("/system/app/GPSToggle.apk").exists()) {
                    Log.i("ProfilesRoot", "Exists");
                    output.writeBytes("rm /system/app/GPSToggle.apk\n");
                    output.flush();
                }
                output.writeBytes("cp " + target + " /system/app/\n");
                output.flush();
                output.writeBytes("chmod 0644 /system/app/GPSToggle.apk\n");
            } else {
                if (new File("/system/priv-app/GPSToggle/GPSToggle.apk").exists()) {
                    Log.i("ProfilesRoot", "Exists");
                    output.writeBytes("rm /system/priv-app/GPSToggle/GPSToggle.apk\n");
                    output.flush();
                } else {
                    output.writeBytes("mkdir -p /system/priv-app/GPSToggle\n");
                    output.flush();
                    output.writeBytes("chmod 0755 /system/priv-app/GPSToggle\n");
                    output.flush();
                }
                output.writeBytes("cp " + target + " /system/priv-app/GPSToggle/\n");
                output.flush();
                output.writeBytes("chmod 0644 /system/priv-app/GPSToggle/GPSToggle.apk\n");
            }
            output.flush();
            output.writeBytes("rm " + target + "\n");
            output.flush();
            output.writeBytes("exit\n");
            output.flush();
            root.waitFor();
            Toast.makeText(this, "Done! Reboot for app to start working!", Toast.LENGTH_LONG).show();
        }catch(Throwable e) {
            Log.e("ProfilesRoot", Log.getStackTraceString(e));
        }
    }

    public void onClickTut(View view) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left)
                .replace(R.id.fragment_container, new ProfilesFragment())
                .commit();
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        tut();
    }

    private final BroadcastReceiver UpdateList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //TODO: Implement init/call fragment
            //init(true);
        }
    };

    public final void tut() {
        try {
            counter = 0;
            scv = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(Target.NONE)
                    .setContentTitle("Welcome")
                    .setContentText("Here's a quick tutorial")
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (counter) {
                                case 0:
                                    scv.setShowcase(new ViewTarget(R.id.fab, ProfilesActivity.this), true);
                                    scv.setContentTitle("Creating profiles");
                                    scv.setContentText("Touch the floating action button to create a profile!");
                                    break;
                                case 1:
                                    scv.setShowcase(new ActionViewTarget(ProfilesActivity.this, ActionViewTarget.Type.OVERFLOW), true);
                                    scv.setContentTitle("Options");
                                    scv.setContentText("Here you can find some handy options");
                                    break;
                                case 2:
                                    scv.setShowcase(new ActionItemTarget(ProfilesActivity.this, R.id.action_clear), true);
                                    scv.setContentTitle("Clear profiles");
                                    scv.setContentText("For example, this option clears all profiles you have saved. Warning: BE CAREFUL!");
                                    break;
                                case 3:
                                    scv.setShowcase(new ActionItemTarget(ProfilesActivity.this, R.id.action_settings), true);
                                    scv.setContentTitle("Settings");
                                    scv.setContentText("There are also additional settings for this app available. Take a look!");
                                    break;
                                case 4:
                                    scv.setShowcase(new ActionItemTarget(ProfilesActivity.this, R.id.action_help), true);
                                    scv.setContentTitle("Help");
                                    scv.setContentText("If you need this tutorial again, or any other help, look here!");
                                    break;
                                case 5:
                                    scv.setShowcase(Target.NONE, false);
                                    scv.setContentTitle("End of tutorial");
                                    scv.setContentText("You're ready! Start creating new profiles, or customising the default ones I've set for you!");
                                    break;
                                case 6:
                                    scv.hide();
                                    if(first_run) {
                                        getSupportFragmentManager().beginTransaction()
                                                .addToBackStack("about")
                                                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                                                .replace(R.id.fragment_container, new AboutFragment())
                                                .commit();
                                    }
                                    break;
                            }
                            counter++;
                        }
                    })
                    .build();
        }catch(Throwable e) {
            Log.e("ProfilesActivityTut", Log.getStackTraceString(e));
        }
    }

    @Override
    public void passName(String profile, boolean isConfig, boolean isRoot) {
        Bundle args = new Bundle();
        args.putString("profile", profile);
        args.putBoolean("root", isRoot);
        if(isConfig) {
            ConfigFragment config = new ConfigFragment();
            config.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                    .replace(R.id.fragment_container, config)
                    .addToBackStack("config")
                    .commit();
        } else {
            Intent wifi = new Intent("haoyangw.List");
            wifi.putExtra("name", profile);
            startActivity(wifi);
        }
    }

    @Override
    public boolean onPreferenceStartScreen(PreferenceFragmentCompat preferenceFragmentCompat, PreferenceScreen preferenceScreen) {
        final Bundle args = new Bundle();
        args.putString(PreferenceFragmentCompat.ARG_PREFERENCE_ROOT, preferenceScreen.getKey());
        final ConfigFragment config = new ConfigFragment();
        config.setArguments(args);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, config, preferenceScreen.getKey())
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_left)
                .addToBackStack(preferenceScreen.getKey())
                .commit();
        return true;
    }

    /**private View.OnClickListener mSelectListener = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
    try {
    final RecyclerView lv = (RecyclerView)view.getParent().getParent();
    final int position = lv.getPositionForView(view);
    final RadioButton select = (RadioButton) view;
    if (prevV != null && !modded) { //TODO: Reduce checking required to secure singleChoice
    if(lv.getPositionForView(prevV) == prev) {
    prevV.setChecked(false);
    //final int prevPos = lv.getPositionForView(prevV);
    selected[prev] = false;
    selected[position] = true;
    select.setChecked(true);//(selected[position]);
    } else {
    Arrays.fill(selected, false);
    selected[position] = true;
    }
    } else{
    Arrays.fill(selected, false);
    selected[position] = true;
    }
    final StringBuilder sb = new StringBuilder();
    final int len = selected.length;
    for (int i = 0; i < len; i++) {
    sb.append(String.valueOf(selected[i]));
    if (i != selected.length - 1) {
    sb.append(",");
    }
    }
    if (prevV == null || modded || lv.getPositionForView(prevV) != prev) {
    mAdapter.notifyDataSetChanged();
    modded = false;
    }
    prevV = select;
    prev = lv.getPositionForView(select); //TODO: Reduce the amount of fields required to secure singleChoice?
    final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
    editor.putString("selected", sb.toString());
    editor.apply();
    //final Intent loadProf = new Intent("com.why.loadProf");
    final Intent loadProf = new Intent(getApplicationContext(), ProfileService.class);
    loadProf.putExtra("name", profList[position]);
    loadProf.putExtra("bg", false);
    startService(loadProf);
    } catch(Throwable e) {
    Log.e("ProfileSelect", Log.getStackTraceString(e));
    }
    }
    };

     private View.OnClickListener mConfigListener = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
    //final int posi = (Integer)view.getTag();
    final RecyclerView lv = (RecyclerView)view.getParent().getParent();
    final int pos = lv.getPositionForView(view);
    final String profName = profList[pos];
    if(!profName.equals("")) {
    modded = true;
    Intent config = new Intent(getApplicationContext(), Config.class);
    //Log.i("ProfilesConfig", String.valueOf(pos));
    //Toast.makeText(getApplicationContext(), prefName, Toast.LENGTH_SHORT).show();
    config.putExtra("name", profName);
    startActivity(config);
    }
    }
    };**/

    /**private static final class ProfilesViewHolder {
     RadioButton select;
     public TextView name;
     }

     private final class ProfileAdapter extends BaseAdapter {
    @Override
    public int getCount() {
    return profList.length;
    }
    @Override
    public String getItem(int position) {
    return profList[position];
    }
    @Override
    public long getItemId(int position) {
    return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    ProfilesViewHolder holder;
    try {
    if (convertView == null) {
    convertView = getLayoutInflater().inflate(R.layout.proflist, parent, false);
    holder = new ProfilesViewHolder();
    holder.select = (RadioButton) convertView.findViewById(R.id.select);
    holder.select.setOnClickListener(mSelectListener);
    holder.name = (TextView) convertView.findViewById(R.id.name);
    final ListView list = (ListView)parent;
    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
    //onListItemClick(view, pos, id);
    onListItemClick(pos);
    }
    });
    list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
    @Override
    public boolean onItemLongClick(AdapterView<?>av, View v, int pos, long id) {
    return onListItemLongClick(pos);
    }
    });
    //convertView.findViewById(R.id.config).setTag(position);
    convertView.findViewById(R.id.config).setOnClickListener(mConfigListener);
    convertView.setTag(holder);
    } else {
    holder = (ProfilesViewHolder) convertView.getTag();
    }
    if (!profiles.equals("")) {
    holder.name.setText(profList[position]);
    }
    if (selected != null) {
    if(selected.length != 0) {
    if (position < selected.length) {
    holder.select.setChecked(selected[position]);
    //if (selected[position]) {
    //prev = position;
    //}
    }
    }
    }
    } catch(Throwable e) {
    Log.e("ProfilegetView", Log.getStackTraceString(e));
    }
    return convertView;
    }
    }**/

}
