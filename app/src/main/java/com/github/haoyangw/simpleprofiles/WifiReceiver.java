package com.github.haoyangw.simpleprofiles;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class WifiReceiver extends BroadcastReceiver{
    //private String lastSsid = "";
    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        //Log.i("Receiver", action);
        try {
            if(action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                final WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                final NetworkInfo netinf = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                //final NetworkInfo.State state = netinf.getState();
                if (netinf.isConnected()) {
                    //if (netinf.getType() == ConnectivityManager.TYPE_WIFI) {
                        final WifiInfo inf = wifi.getConnectionInfo();
                        if (inf != null) {
                            final String ssid = inf.getSSID().replace("\"", "");
                            if(!ssid.equals("")) {
                                SharedPreferences.Editor edit = prefs.edit();
                                edit.putString("lastssid", ssid);
                                edit.apply();
                                if (prefs.contains(ssid + "con")) {
                                    // There is a profile to activate
                                    final String prof = prefs.getString(ssid + "con", "");
                                    if (!prof.equals("")) {
                                        final Intent act = new Intent(context, ProfileService.class);
                                        act.putExtra("name", prof);
                                        act.putExtra("bg", true);
                                        Log.i("ProfileReceiver", "Activating profile '" + prof + "'" + " on connect to " + ssid);
                                        context.startService(act);
                                    }
                                }
                                //lastSsid = ssid;
                                //Log.i("ReceiverConnect", ssid);
                            }
                            //Toast.makeText(context, ssid, Toast.LENGTH_SHORT).show();
                        }
                    //}
                } else {
                    final String lastSsid = prefs.getString("lastssid", "");
                    if (!lastSsid.equals("")) {
                        //Log.i("ReceiverDisconnect", lastSsid);
                        if(prefs.contains(lastSsid + "dis")) {
                            final String prof = prefs.getString(lastSsid + "dis", "");
                            if (!prof.equals("")) {
                                final Intent act = new Intent(context, ProfileService.class);
                                act.putExtra("name", prof);
                                act.putExtra("bg", false);
                                Log.i("ProfileReceiver", "Activating profile '" + prof + "'" + " on disconnect from " + lastSsid);
                                context.startService(act);
                            }
                        }
                        prefs.edit().putString("lastssid", "").apply();
                    }
                }
            }
        /**if(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION.equals(action)) {
            SupplicantState supp = intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
            if(SupplicantState.isValidState(supp) && supp == SupplicantState.COMPLETED) {
                final WifiManager wifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo inf = wifi.getConnectionInfo();
                if(inf != null) {
                    final String ssid = inf.getSSID();
                    Log.i("Receiver", ssid);
                    Toast.makeText(context, ssid, Toast.LENGTH_SHORT).show();
                }
            }
        }**/
        }catch(Throwable e) {
            Log.e("ProfileWifiReceiver", Log.getStackTraceString(e));
        }
    }
}
