package com.github.haoyangw.simpleprofiles.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.haoyangw.simpleprofiles.R;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NetworkList extends Fragment {
    private String[] ssids = null;
    private String prof = "";
    private Context mContext;

    public static NetworkList newInstance(String profile) {
        NetworkList nl = new NetworkList();
        Bundle arg = new Bundle();
        arg.putString("name", profile);
        nl.setArguments(arg);
        return nl;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_network, container, false);
        getSsids(getActivity());
        final RecyclerView mList = (RecyclerView) rootView.findViewById(R.id.list);
        final TextView empt = (TextView)rootView.findViewById(R.id.empt_net);
        if(mList != null) {
            final NetworkAdapter mAdapter = new NetworkAdapter();
            mList.setAdapter(mAdapter);
            mList.setLayoutManager(new LinearLayoutManager(getActivity()));
            mList.setHasFixedSize(true);
            mList.addItemDecoration(new RCDivider(getActivity()));

        }
        if(ssids.length == 0 || ssids == null) {
            empt.setVisibility(View.VISIBLE);
        }
        Bundle args = getArguments();
        prof = args.getString("name");
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("'" + prof + "': Wifi Triggers" );
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(menu != null) {
            menu.findItem(R.id.action_about).setVisible(false);
            menu.findItem(R.id.action_clear).setVisible(false);
            menu.findItem(R.id.action_help).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(false);
            menu.findItem(R.id.stop_boot).setVisible(false);
            menu.findItem(R.id.debug_al).setVisible(false);
            menu.findItem(R.id.rem_al).setVisible(false);
            menu.findItem(R.id.action_exit).setVisible(false);
        }
    }

    public final class NetworkAdapter extends RecyclerView.Adapter<NetworkAdapter.NetworkHolder> {

        @Override
        public NetworkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View row = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.wifilist, parent, false);
            return new NetworkHolder(row);
        }

        @Override
        public void onBindViewHolder(NetworkHolder holder, int position) {
            holder.ssid.setText(ssids[position]);
        }

        @Override
        public int getItemCount() {
            return ssids.length;
        }

        final class NetworkHolder extends RecyclerView.ViewHolder {
            private TextView ssid;

            NetworkHolder(View view) {
                super(view);
                ssid = (TextView)view.findViewById(R.id.ssid);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onListItemClick(getAdapterPosition());
                    }
                });
            }
        }
    }

    protected final void onListItemClick(int position) {
        final int pos = position;
        AlertDialog.Builder select = new AlertDialog.Builder(mContext);
        final String name = ssids[pos];
        select.setTitle(name)
                .setItems(R.array.pref_trigger, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //final String name = (String)lv1.getItemAtPosition(pos);
                        //if (!ssids[pos].equals(" ")) {
                        if(!name.equals("")) {
                            final SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                            //String profs;
                            //final StringBuilder sb = new StringBuilder();
                            switch (i) {
                                case 0:
                                    /**profs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(name + "con", "");
                                     if (!profs.equals("")) {
                                     sb.append(profs).append(",");
                                     }
                                     sb.append(prof);**/
                                    Toast.makeText(mContext, "Will trigger '" + prof + "' once connected to '" + name + "'", Toast.LENGTH_SHORT).show();
                                    edit.putString(name + "con", prof);
                                    break;
                                case 1:
                                    /**profs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(name + "dis", "");
                                     if (!profs.equals("")) {
                                     sb.append(profs).append(",");
                                     }
                                     sb.append(prof);**/
                                    Toast.makeText(mContext, "Will trigger '" + prof + "' once disconnected from '" + name + "'", Toast.LENGTH_SHORT).show();
                                    edit.putString(name + "dis", prof);
                                    break;
                            }
                            edit.apply();
                            //profArr = sb.toString().split(",");
                        }
                    }
                }).create().show();
    }

    private void getSsids(Context context) {
        final WifiManager wifiman = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        final List<WifiConfiguration> list = wifiman.getConfiguredNetworks();
        if(list != null) {
            ssids = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).SSID != null && !(list.get(i).SSID.trim().isEmpty())) {
                    ssids[i] = list.get(i).SSID.replace("\"", "");
                    //Log.i("Network", list.get(i).SSID);
                }
            }
            final List<String> tmp = Arrays.asList(ssids);
            Collections.sort(tmp, new Comparator<String>() {
                @Override
                public int compare(String s1, String s2) {
                    return s1.compareToIgnoreCase(s2);
                }
            });
            for (int i = 0; i < tmp.size(); i++) {
                ssids[i] = tmp.get(i);
            }
        } else {
            ssids = new String[0];
        }
    }
}
