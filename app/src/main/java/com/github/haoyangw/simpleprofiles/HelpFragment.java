package com.github.haoyangw.simpleprofiles;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends Fragment {


    public HelpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_help, container, false);
        if(Build.VERSION.SDK_INT < 19 && Build.VERSION.SDK_INT >= 17) {
            final Button install = (Button)view.findViewById(R.id.install);
            if (install != null) {
                install.setVisibility(View.GONE);
            }
        }
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.action_help);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(menu != null) {
            menu.findItem(R.id.action_about).setVisible(false);
            menu.findItem(R.id.action_clear).setVisible(false);
            menu.findItem(R.id.action_help).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(false);
            menu.findItem(R.id.stop_boot).setVisible(false);
            menu.findItem(R.id.debug_al).setVisible(false);
            menu.findItem(R.id.rem_al).setVisible(false);
            menu.findItem(R.id.action_exit).setVisible(false);
        }
    }

}
